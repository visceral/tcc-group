<?php
$featured_image        = App\get_aspect_ratio_image(2, 1, 'medium');
$resource_types        = wp_get_post_terms(get_the_id(), 'resource_type', array('fields' => 'names') );
$resource_types_output = '';
$card_image_class      = has_post_thumbnail() ? 'list-item-card--featured-image' : '';

if (!empty($resource_types)) {
    foreach ($resource_types as $resource_type) {
        $resource_types_output .= $resource_type . ', ';
    }

    $resource_types_output = substr($resource_types_output, 0, -2);
}
?>
<article class="list-item-card list-item-card--resource column xs-100 md-50 lg-33 reveal <?php echo $card_image_class; ?>">
    <div>
        <?php if (has_post_thumbnail()) { ?>
            <div class="image-zoom">
                <div class="list-item-card__image img-cover">
                    <?php echo $featured_image; ?>
                </div>
            </div>    
        <?php } ?>
        <h3 class="list-item-card__title"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
        <p class="list-item-card__excerpt small"><?php echo wp_trim_words( get_the_excerpt(), 20 ); ?></p>
        <?php if ($resource_types_output) { ?>
            <div class="list-item-card__meta">
                <span class="meta-post-type"><?php echo $resource_types_output; ?></span>
            </div>
        <?php } ?>
    </div>
</article>