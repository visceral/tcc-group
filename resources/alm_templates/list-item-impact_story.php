<?php
$featured_image = App\get_aspect_ratio_image(6, 4, '600x400');
$client_name = get_post_meta(get_the_id(), 'client_name', true);
?>

<div class="column xs-100 md-50">
    <article class="list-item-story reveal">
        <a href="<?php echo get_permalink(); ?>" class="list-item-story__link image-zoom">
            <div class="list-item-story__image img-cover">
                <?php
                if (has_post_thumbnail()) {
                    echo $featured_image;
                }
                ?>
            </div>
            <div class="list-item-story__content text-white">
                <h2 class="list-item-story__title"><?php echo get_the_title(); ?></h2>
                <p class="list-item-story__meta"><?php echo $client_name; ?></p>
            </div>
        </a>
    </article>
</div> 