// import external dependencies
import 'jquery';

// Import everything from autoload
import "./autoload/**/*"

// Import Slick
import 'slick-carousel/slick/slick.min';
// Import Waypoints
import 'waypoints/lib/jquery.waypoints.min';
// Import YT Player
// import 'jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import singleResource from './routes/single-resource';
import singlePerson from './routes/single-person';
import singleProduct from './routes/single-product';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  // Single Resource pages
  singleResource,
  // Single Person pages
  singlePerson,
  // Single Product pages
  singleProduct,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
