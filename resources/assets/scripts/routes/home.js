export default {
    init() {
        // JavaScript to be fired on the home page
        /** VIDEO BG */
        if (window.innerWidth > 768) {
            $(".player").YTPlayer();
        }

        // slick slide - tweets
        if ($('.ctf-tweets').length > 0) {
            var slider = $('.ctf-tweets');

            slider.slick({
                slide: '.ctf-item',
                autoplay: true,
                autoplaySpeed: 4000,
                infinite: true,
                centerMode: true,
                centerPadding: '0px',
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                dots: true,
                arrows: false,
                fade: true,
                mobileFirst: true,
            });
        }

        // const player = document.getElementById('home-masthead-video');
        const playerWrap = document.querySelector('.home-intro-slide-video-bg');
        // player.addEventListener('play', () => {
        window.addEventListener('load', () => {
            console.log('playing!');
            if (playerWrap) {
                playerWrap.classList.add('playing');
            }
        });
        // player.play().catch(() => {
        //     console.log('playback failed, muting to try again');
        //     player.muted = true;
        //     player.play();
        // });
    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
    },
};
