export default {
  init() {
    // JavaScript to be fired on each resource

    /** COOKIE FUNCTIONS */
    function createCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    /*
    function eraseCookie(name) {
        createCookie(name,"",-1);
    }
    */
    /** END COOKIE FUNCTIONS */


    var $modalBG = $('.modal-bg');
    var $resourceURL;
    var downloadGate = readCookie('tcc_resource_download_gate');
    $('.single-resource-content__links-container a.resource-link--gated').on('click', function(e) {
        // Check if it's an internal link 
        if (this.href.indexOf(window.location.host)) {
            console.log('link is internal');
            
            if ( downloadGate === null ) {
                console.log('no gate cookie present');
                e.preventDefault();
                $resourceURL = $(this).attr('href');
                $modalBG.show();
            }
        }	
    });

    // Close modal on close icon click
    $('.download-modal .icon-close').on('click', function() {
        $modalBG.hide();
    });

    // Close modal on modal background click
    $modalBG.on('click', function() {
        $modalBG.hide();
    });

    // Prevent modal closing when modal is clicked 
    // (it is a child of the modal background and we don't want the event to bubble up to affect the parent)
    $('.download-modal').on('click', function(e) {
        e.stopPropagation();
    });

    $(document).bind('gform_confirmation_loaded', function(){
        createCookie('tcc_resource_download_gate', '', 365);
        downloadGate = false;
        window.open($resourceURL, '_blank');
    });	

    $('.resource-download').bind('contextmenu', function() {
        return false;
    });
  },
};
