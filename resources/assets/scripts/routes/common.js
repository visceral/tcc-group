export default {
    init() {
        // JavaScript to be fired on all pages

        /** GLOBAL VARIABLES */
        var body = document.querySelector('body');
        /** END GLOBAL VARIABLES */

        /** SKIP LINK NAVIGATION */
        $('#skip-to-content').click(function () {
            // strip the leading hash and declare
            // the content we're skipping to
            var skipTo = "#" + this.href.split('#')[1];
            // Setting 'tabindex' to -1 takes an element out of normal 
            // tab flow but allows it to be focused via javascript
            $(skipTo).attr('tabindex', -1).on('blur focusout', function () {
                // when focus leaves this element, 
                // remove the tabindex attribute
                $(this).removeAttr('tabindex');
            }).focus(); // focus on the content container
        });
        /** END SKIP LINK NAVIGATION */

        /** RESPONSIVE NAV OPEN/CLOSE **/
        var mobileNav = document.querySelector('#mobile-nav');
        var mobileNavButton = document.querySelector('#mobile-nav-icon');

        // Toggle menu icon
        if (mobileNavButton) {
            mobileNavButton.addEventListener('click', function () {
                body.classList.toggle('mobile-nav-open');
            });
        }

        // Close menu if page is clicked (not menu item)
        if (mobileNav) {
            mobileNav.addEventListener('click', function (e) {
                // Don't hide if click is on a child element (nav link)
                if (e.target !== this) {
                    return;
                }
                mobileNavButton.classList.toggle('opened');
                $("#nav-toggle").prop('checked', false);
            });

            // Disable click on mobile
            $("#mobile-nav .menu-item-has-children > .sub-menu--open").on("click", function () {
                $(this).parent('.menu-item').toggleClass("mobile-nav--opened");
            });
        }
        /** END RESPONSIVE NAV OPEN/CLOSE **/

        /** SHOW AND HIDE SEARCH BAR **/
        var searchButton = $('.header__search-button'),
            headerSearch = $('.header__search'),
            searchClose = $('.header__search-close');

        searchButton.click(function () {
            headerSearch.slideToggle('slow', function () {
                if ((window.ActiveXObject) && !("ActiveXObject" in window)) {
                    headerSearch.find('input[type="text"]').focus();
                }
            });
            return false;
        });

        searchClose.click(function () {
            headerSearch.slideToggle('fast');
        });

        // Shake Animation
        function shake(div) {
            var interval = 100;
            var distance = 10;
            var times = 4;

            $(div).css('position', 'relative');
            for (var iter = 0; iter < (times + 1); iter++) {
                $(div).animate({ left: ((iter % 2 === 0 ? distance : distance * -1)) }, interval);
            }
            $(div).animate({ left: 0 }, interval);
        }

        // Search Submit
        headerSearch.submit(function (e) {
            var s = $(this).find('input[type="text"]');
            if (!s.val()) {
                e.preventDefault();
                shake($(this).find('input[type="text"]'));
                $(this).find('input[type="text"]').focus();
            }
        });
        /** END SHOW OR HIDE SEARCH BAR **/

        /** MASTHEAD PRGRESSIVE IMAGES **/
        var masthead = document.querySelector('.masthead'),
            placeholderOverlay = document.querySelector('.masthead__overlay');

        if (masthead && placeholderOverlay) {
            // Load full size image. When loaded, fade our placeholder add it as bg to masthead
            var img = new Image();
            img.src = masthead.dataset.imageSrc;
            img.onload = function () {
                placeholderOverlay.classList.add('fade-out');
                masthead.style.backgroundImage = "url(" + img.src + ")";
            };
        }
        /** END MASTHEAD PRGRESSIVE IMAGES **/

        /* HEADER SCROLL EFFECT */
        // Reference: http://tympanus.net/codrops/2013/06/06/on-scroll-animated-header/
        var animatedHeader = (function () { // eslint-disable-line no-unused-vars
            var didScroll = false,
                scrollOffset = 0;
            function scrollY() {
                return window.pageYOffset || document.documentElement.scrollTop;
            }
            function scrollPage() {
                var sy = scrollY();
                if (sy > scrollOffset) {
                    body.classList.add('scroll-triggered');
                }
                else {
                    body.classList.remove('scroll-triggered');
                }
                didScroll = false;
            }
            function init() {
                window.addEventListener('load', scrollPage);
                window.addEventListener('scroll', function (event) { // eslint-disable-line no-unused-vars
                    if (!didScroll) {
                        didScroll = true;
                        setTimeout(scrollPage, 250);
                    }
                }, false);
            }
            init();
        })();
        /* END HEADER SCROLL EFFECT */

        /** ANIMATED ANCHOR LINKS **/
        $('a[href*="#"]:not([href="#"])').click(function (e) {
            if (!$(this).closest('li').hasClass('vc_tta-tab')) {
                if (!$(this).closest('h4').hasClass('vc_tta-panel-title')) {
                    if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                        var target = $(this.hash);
                        var $this = this;
                        var header = $('header.header');
                        var wpAdminBar = $('#wpadminbar');
                        var fixedHeaderOffset = parseInt(header.outerHeight()) - parseInt(header.css("padding-top").replace("px", ""));
                        // If we're logged in and WP ADmin Bar exists, add it to the offset
                        if (wpAdminBar.length) {
                            fixedHeaderOffset += parseInt(wpAdminBar.outerHeight());
                        }
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: (target.offset().top - fixedHeaderOffset),
                            }, 500, function () {
                                if (history.pushState) {
                                    history.pushState(null, null, $this.hash);
                                }
                                else {
                                    location.hash = $this.hash;
                                }
                            });
                            e.preventDefault();
                        }
                    }
                }
            }
        });
        /** END ANIMATED ANCHOR LINKS **/

        // Reveal animation

        function itemReveal() {
            var waypoints = $('.reveal').waypoint({ // eslint-disable-line no-unused-vars
                handler: function (direction) { // eslint-disable-line no-unused-vars
                    $(this)[0].element.classList.add('revealed');
                },
                offset: '85%',
            });
        }

        itemReveal();

        // Re-runs waypoints after ajax load more is finished
        $.fn.almComplete = function (alm) { // eslint-disable-line no-unused-vars
            itemReveal();
        };

        // slick slide
        if ($('.visceral-slider').length > 0) {
            var slider = $('.visceral-slider > .wpb_column > .vc_column-inner > .wpb_wrapper');

            slider.slick({
                slide: '.visceral-slider > .wpb_column > .vc_column-inner > .wpb_wrapper > *',
                autoplay: true,
                autoplaySpeed: 8000,
                infinite: true,
                centerMode: true,
                centerPadding: '0px',
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                dots: false,
                arrows: true,
                fade: true,
                mobileFirst: true,
                prevArrow: '<button type="button" class="slick-prev"><span class="icon icon-chevron-left"></span><span class="screen-reader-text">Previous</span></button>',
                nextArrow: '<button type="button" class="slick-next"><span class="icon icon-chevron-right"></span><span class="screen-reader-text">Next</span></button>',
            });
        }

        // Mega Nav
        $('.header__navigation .nav > .menu-item-has-children > a').on('click', function (e) {
            e.preventDefault();

            var $nav = $('.header__navigation .nav');
            var $li = $(this).parents('.menu-item');
            var $fade = $(this).parents('.menu-item').find('.menu-depth-1 .row > .menu-item');

            // close any other open sub-menus
            $nav.find('li').not($li).find('.header__sub-menu.menu-depth-1').removeClass('menu-item--active');
            $nav.find('li').not($li).find('.header__sub-menu.menu-depth-1 .menu-item').removeClass('menu-item--fade');
            // Removes active state to list item
            $nav.find('li').not($li).removeClass('menu-item--active');

            // Removes active class and adds it to the one clicked
            // $('.header__navigation .nav > li').each(function() {
            //     $(this).removeClass('menu-item--active');
            // });
            if ($li.hasClass('menu-item--active')) {
                $li.removeClass('menu-item--active');
                $fade.removeClass('menu-item--fade');
            } else {
                $li.addClass('menu-item--active');
                $fade.addClass('menu-item--fade');
            }
        });

        $('.header__navigation .menu-item-has-children').keypress(function (e) {
            if ((e.keyCode ? e.keyCode : e.which) == 13) {
                if ($(this).is(":focus")) {
                    $(this).find('.header__sub-menu').slideToggle();
                }
            }
        });

        // Move input description on the WooCommerce Checkout page
        var $orgLeadDescription = $('#org_lead_email_field .description');
        var $orgLeadNameContainer = $('#org_lead_name_field');
        if ($orgLeadDescription && $orgLeadNameContainer) {
            $orgLeadNameContainer.prepend($orgLeadDescription);
        }

    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
