export default {
    init() {
      // JavaScript to be fired on a single person
      $('.related-content__link').on('click', function(e) {
          e.preventDefault();
        $(this).hide();
        $(this).closest('p').next('.related-content__more').slideToggle();
      });
    },
  };