@extends('layouts.app')
@section('content')
@while(have_posts()) @php(the_post())
@include('partials.content-page')
@php
$form_filters = App::formFilters();
@endphp
@if($featured_impact_story)
<article class="featured-impact-story">
    <a href="{{ get_permalink($featured_impact_story->ID) }}" class="featured-impact-story__link image-zoom">
        @if(has_post_thumbnail($featured_impact_story->ID))
        <div class="featured-impact-story__image img-cover">
            {{ $featured_impact_story_featured_image }}
            @php
            echo get_the_post_thumbnail( $featured_impact_story->ID, 'large', array('class' => 'portrait') );
            @endphp
        </div>
        @endif
        <div class="featured-impact-story__content text-white">
            <h2 class="featured-impact-story__title">{{ get_the_title($featured_impact_story->ID) }}</h2>
            <p class="featured-impact-story__meta">{{ get_post_meta($featured_impact_story->ID, 'client_name', true) }}
            </p>
        </div>
    </a>
</article>
@endif
<form action="{{ get_permalink() }}">
    <div class="row">
        <div class="column xs-100 md-25">
            <label class="screen-reader-text" for="story_sector">{{ __('Sector', 'visceral') }}</label>
            <select name="story_sector" id="story_sector" class="input--alt">
                <option value="">{{ __('All Sectors', 'visceral') }}</option>
                @if($get_impact_story_sectors)
                @foreach($get_impact_story_sectors as $story_sector)
                <option value="{{ $story_sector->slug }}" {{$form_filters->story_sector === $story_sector->slug ?
                    'selected' : ''}}>{{ $story_sector->name }}</option>
                @endforeach
                @endif;
            </select>
        </div>
        <div class="column">
            <input type="submit" value="Submit">
        </div>
    </div>
</form>
@if($get_impact_stories->posts)
<section class="story-list">
    <div class="row">
        @php
        // var_dump($get_impact_stories);

        @endphp

        @foreach($get_impact_stories->posts as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))

        @include('partials.list-item-impact_story')
        @php(wp_reset_postdata())
        @endforeach
    </div>
</section>
@includeWhen($get_impact_stories->max_num_pages > 1, 'partials.load-more')
@endif
@endwhile
@endsection