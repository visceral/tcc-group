@extends('layouts.app')
@section('content')
@while(have_posts()) @php(the_post())
@include('partials.content-page')
@php
$form_filters = App::formFilters();
@endphp
<form action="{{ get_permalink() }}" class="filters filters--resources">
    <div class="row">
        <div class="column xs-100 md-50 lg-25">
            <label class="screen-reader-text" for="resource_keyword">{{ __('Keyword Search', 'visceral') }}</label>
            <input type="text" name="resource_keyword" id="resource_keyword" class="input--alt input--search"
                placeholder="Keyword Search" value="{{$form_filters->resource_keyword}}">
        </div>
        <div class="column xs-100 md-50 lg-25">
            <label class="screen-reader-text" for="resource_type">{{ __('Type', 'visceral') }}</label>
            <select name="resource_type" id="resource_type" class="input--alt">
                <option value="">{{ __('All Types', 'visceral') }}</option>
                @if($get_resource_types)
                @foreach($get_resource_types as $resource_type)
                @php( $resource_type_selected = ($form_filters->resource_type === $resource_type->slug ) ? 'selected' :
                '')
                <option value="{{ $resource_type->slug }}" {{ $resource_type_selected }}>{{ $resource_type->name }}
                </option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="column xs-100 md-50 lg-25">
            <label class="screen-reader-text" for="resource_sector">{{ __('Sector', 'visceral') }}</label>
            <select name="resource_sector" id="resource_sector" class="input--alt">
                <option value="">{{ __('All Sectors', 'visceral') }}</option>
                @if($get_resource_sectors)
                @foreach($get_resource_sectors as $resource_sector)
                @php( $resource_sector_selected = ($form_filters->resource_sector === $resource_sector->slug ) ?
                'selected' : '')
                <option value="{{ $resource_sector->slug }}" {{ $resource_sector_selected }}>{{ $resource_sector->name
                    }}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="column">
            <input type="submit" value="Submit">
        </div>
    </div>
</form>
@if($featured_resource)
<article class="featured-resource reveal">
    <a href="{{ get_permalink($featured_resource->ID) }}" class="featured-resource__link image-zoom">
        @if(has_post_thumbnail($featured_resource->ID))
        <div class="featured-resource__image img-cover">
            {{ $featured_resource_featured_image }}
            @php
            echo get_the_post_thumbnail( $featured_resource->ID, 'large', array('class' => 'portrait') );
            @endphp
        </div>
        @endif
        <div class="featured-resource__content text-white">
            <div class="row">
                <div class="column md-75 lg-50">
                    <p class="featured-resource__meta">{{ $featured_resource_types }}</p>
                    <h2 class="featured-resource__title">{{ get_the_title($featured_resource->ID) }}</h2>
                </div>
            </div>
        </div>
    </a>
</article>
@endif
@if($get_resources->posts)
<section class="resource-list">
    <div class="row">
        @php
        global $post;
        $page_slug = $post->post_name;
        @endphp
        @foreach($get_resources->posts as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))

        @include('partials.list-item-card')
        @php(wp_reset_postdata())
        @endforeach
    </div>
</section>
@endif
@includeWhen($get_resources->max_num_pages > 1, 'partials.load-more')

<div class="results-pagination">
    {!! $pagination !!}
</div>
@endwhile
@endsection