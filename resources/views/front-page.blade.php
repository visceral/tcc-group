@extends('layouts.app')

@section('content')
    @while(have_posts()) @php(the_post())
        <section class="homepage-masthead text-white" {!! $homepage_video_fallback_image !!}>
            {!! $homepage_video !!}
            <div class="homepage-masthead__inner">
                <div class="homepage-masthead__headline homepage-masthead__headline--first reveal reveal--up">
                    <div class="container">
                        {!! $first_headline !!}
                    </div>
                    
                </div>
            </div>
            <div class="homepage-masthead-promos">
                <div class="container">
                    <div class="row">
                        @if($homepage_masthead_promos)
                            @foreach($homepage_masthead_promos as $homepage_masthead_promo) 
                                <div class="homepage-masthead-promos__promo column">
                                    <div class="homepage-masthead-promos__promo__inner">
                                        <a href="{{$homepage_masthead_promo['promo_url']['url']}}">{{$homepage_masthead_promo['promo_text']}}<i class="icon icon-right-arrow" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <a id="mouse-scroll" href="#homepage-content">
                            <span class="screen-reader-text">{{__('Scroll to main content.', 'visceral')}}</span>
                            <svg  width="33px" height="76px" viewBox="0 0 33 76" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <defs>
                                    <polygon id="path-1" points="0 0.401837838 33 0.401837838 33 39.5097656 33 53.9997568 0 53.9997568"></polygon>
                                </defs>
                                <g id="Designs-R3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="0.0-Hompage-2" transform="translate(-1335.000000, -795.000000)">
                                        <g id="Scroll-Down" transform="translate(1335.000000, 795.000000)">
                                            <path id="mouse-scroll__arrows" d="M21.0039047,68.3350076 L16.0000009,73.2663314 L10.9960935,68.3350076 C10.7591131,68.1027328 10.4856784,67.9865971 10.1757811,67.9865971 C9.86588379,67.9865971 9.59244902,68.1027328 9.35546871,68.3350076 C9.11848839,68.5672823 9,68.8352856 9,69.1390277 C9,69.4427698 9.11848839,69.7107731 9.35546871,69.9430479 L15.179685,75.6515913 C15.3072915,75.7766617 15.4485654,75.8659964 15.6035139,75.919598 C15.7584624,75.9731996 15.8906224,76 16.0000009,76 C16.1093759,76 16.2415358,75.9731996 16.3964843,75.919598 C16.5514328,75.8659964 16.6927068,75.7766617 16.8203098,75.6515913 L22.6445295,69.9430479 C22.881511,69.7107731 23,69.4427698 23,69.1390277 C23,68.8352856 22.881511,68.5672823 22.6445295,68.3350076 C22.4075481,68.1027328 22.1341141,67.9865971 21.8242171,67.9865971 C21.5143202,67.9865971 21.2408862,68.1027328 21.0039047,68.3350076 Z M15.179685,67.664989 C15.3072915,67.7721951 15.4485654,67.8525954 15.6035139,67.9061968 C15.7584624,67.9597981 15.8906224,67.9865971 16.0000009,67.9865971 C16.1093759,67.9865971 16.2415358,67.9597981 16.3964843,67.9061968 C16.5514328,67.8525954 16.6927068,67.7721951 16.8203098,67.664989 L22.6445295,61.9296484 C22.881511,61.6973737 23,61.4293704 23,61.1256248 C23,60.8218827 22.881511,60.5538794 22.6445295,60.3216081 C22.4075481,60.1071993 22.1341141,60 21.8242171,60 C21.5143202,60 21.2408862,60.1071993 21.0039047,60.3216081 L16.0000009,65.2529319 L10.9960935,60.3216081 C10.7591131,60.1071993 10.4856784,60 10.1757811,60 C9.86588379,60 9.59244902,60.1071993 9.35546871,60.3216081 C9.11848839,60.5538794 9,60.8218827 9,61.1256248 C9,61.4293704 9.11848839,61.6973737 9.35546871,61.9296484 L15.179685,67.664989 Z" id="Fill-1" fill="#FFFFFF"></path>
                                            <path d="M17,20 C15.899928,20 15,19.1032609 15,18.007087 L15,10.992913 C15,9.89673913 15.899928,9 17,9 C18.100072,9 19,9.89673913 19,10.992913 L19,18.007087 C19,19.1032609 18.100072,20 17,20" id="Fill-1" fill="#FEFEFE"></path>
                                            <path d="M0,17.6718649 L0,36.8525676 C0,45.6895946 6.37504225,52.9927297 14.5632254,53.9997568 L14.5632254,53.0194865 C6.88909859,52.0178108 0.929577465,45.1525135 0.929577465,36.8525676 L0.929577465,17.6718649 C0.929577465,8.68548649 7.91442254,1.37456757 16.5,1.37456757 C25.0855775,1.37456757 32.0704225,8.68548649 32.0704225,17.6718649 L32.0704225,36.8525676 C32.0704225,45.1525135 26.1109014,52.0178108 18.4367746,53.0194865 L18.4367746,53.9997568 C26.6249577,52.9927297 33,45.6895946 33,36.8525676 L33,17.6718649 C33,8.14889189 25.5982394,0.401594595 16.5,0.401594595 C7.40176056,0.401594595 0,8.14889189 0,17.6718649 Z" id="Fill-3" fill="#FEFEFE"></path>
                                            <mask id="mask-2" fill="white">
                                                <use xlink:href="#path-1"></use>
                                            </mask>
                                            <g id="Clip-4"></g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>    
                    </div>
                </div> 
            </div>    
        </section>
        <section id="homepage-content">
            <div class="container">
                @php(the_content())
            </div>
        </section>
    @endwhile
@endsection
