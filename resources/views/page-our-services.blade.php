@extends('layouts.app') 
@section('content') 
    @while(have_posts()) @php(the_post())
        @include('partials.content-page')
        @if($get_services->posts)
            <section class="resource-list">
                <div class="row">
                    @foreach($get_services->posts as $post)  
                        @php(setup_postdata($GLOBALS['post'] = $post)) 
                    
                        @include('partials.list-item-'.get_post_type(), $post)
                        @php(wp_reset_postdata())
                    @endforeach
                </div>    
            </section>
            <section>
                {{ the_content() }}
            </section>
        @endif 
    @endwhile
@endsection