@extends('layouts.app')
@section('content')
@while(have_posts()) @php(the_post())
@include('partials.content-page')
@php
$form_filters = App::formFilters();
@endphp
<form action="" class="filters filters--posts">
    <div class="row">
        <div class="column xs-100 md-50 lg-25">
            <label class="screen-reader-text" for="type">Type</label>
            <select name="type" id="type" class="input--alt">
                <option value="">{{ __('All Types', 'visceral') }}</option>
                @if($get_post_types)
                @foreach($get_post_types as $type)
                @php( $type_selected = ($form_filters->type === $type->slug ) ? 'selected' : '')
                <option value="{{ $type->slug }}" {{ $type_selected }}>{{ $type->name }}</option>
                @endforeach
                @endif;
            </select>
        </div>
        <div class="column xs-100 md-50 lg-25">
            <label class="screen-reader-text" for="sector">Sector</label>
            <select name="sector" id="sector" class="input--alt">
                <option value="">{{ __('All Sectors', 'visceral') }}</option>
                @if($get_post_sectors)
                @foreach($get_post_sectors as $sector)
                @php( $sector_selected = ($form_filters->sector === $sector->slug ) ? 'selected' : '')
                <option value="{{ $sector->slug }}" {{ $sector_selected }}>{{ $sector->name }}</option>
                @endforeach
                @endif;
            </select>
        </div>
        <div class="column">
            <input type="submit" value="Submit">
            <a href="{{ bloginfo('rss2_url') }}" class="rss-link"><i class="icon icon-feed" aria-hidden="true"></i>{{
                __('Subscribe', 'visceral') }}</a>
        </div>
    </div>
    {{-- @php(var_dump($get_departments)); --}}
</form>
@if($get_posts->posts)
<section class="list-wrap list-wrap--post">
    <div class="row justify-center">
        @foreach($get_posts->posts as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))

        @include('partials.list-item-'.get_post_type())
        @php(wp_reset_postdata())
        @endforeach
    </div>
</section>
@includeWhen($get_posts->max_num_pages > 1, 'partials.load-more')

<div class="results-pagination">
    {!! $pagination !!}
</div>
@endif
@endwhile
@endsection