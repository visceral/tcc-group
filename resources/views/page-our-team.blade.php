@extends('layouts.app')
@section('content')
@while(have_posts()) @php(the_post())
@include('partials.content-page')

@if($get_leadership)
<section class="person-list">
    <h2 class="h1 person-list__heading">{{ __('Leadership', 'visceral')}}</h2>
    <div class="row">
        @foreach($get_leadership->posts as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))

        @include('partials.list-item-'.get_post_type())
        @php(wp_reset_postdata())
        @endforeach
    </div>
</section>
@endif
@if($get_staff)
<section class="person-list">
    <h2 class="h1 person-list__heading">{{ __('Staff', 'visceral')}}</h2>
    <div class="row">
        @foreach($get_staff->posts as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))

        @include('partials.list-item-'.get_post_type())
        @php(wp_reset_postdata())
        @endforeach
    </div>
</section>
@endif
@if($get_affiliates)
<section class="person-list">
    <h2 class="person-list__heading h1">{{ __('Affiliates', 'visceral')}}</h2>
    <div class="row">
        @foreach($get_affiliates->posts as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))

        @include('partials.list-item-'.get_post_type())
        @php(wp_reset_postdata())
        @endforeach
    </div>
</section>
@endif
@endwhile
@endsection