@php
$featured_image = App\get_aspect_ratio_image(10, 4, 'large');

$categories = wp_get_post_terms(get_the_id(), 'category', array('fields' => 'names') );
$categories_output = '';

if (!empty($categories)) {
    foreach ($categories as $category) {
        $categories_output .= $category . ', ';
    }

    $categories_output = substr($categories_output, 0, -2);
}

$authors_image_output = '';
$authors_name_output  = '';
$multi_author_class   = '';
$total_authors_array  = [];

if ((function_exists('get_field') && !empty(get_field('authors'))) || !empty(get_field('custom_authors'))) {
    $author_names_array = array();
    $author_images = '<div class="list-item--post__authors__images">';
    // Staff Authors
    if (function_exists('get_field') && !empty(get_field('authors'))) {
        $authors = get_field('authors');

        foreach ($authors as $author) {
            $ID = $author->ID;
            $published = $author->post_status === 'publish' ? true : false;
            $total_authors_array[] = $author;
            
            if ($published) {
                $authors_name_output .= $author->post_title . ', ';
                if (has_post_thumbnail($ID)) { 
                    $authors_image_output .= '<div class="img-circle">';
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($ID), 'medium');
                        $w     = $image[1];
                        $h     = $image[2];
                        $author_img_class = ( $w < $h ) ? 'portrait' : '';
                    
                    $authors_image_output .= get_the_post_thumbnail($ID, 'thumbnail', array('class' => $author_img_class)); 
                    $authors_image_output .= '</div>';
                }
            }
        }
    }

    // Custom Authors
    if (function_exists('have_rows') && !empty(have_rows('custom_authors'))) {
        while ( have_rows('custom_authors') ) : the_row();
            // Check if name provided (don't show image if no name)
            if (get_sub_field('custom_author_name') !== "") {
                $custom_author_name = get_sub_field('author_name');
                $authors_name_output .= $custom_author_name . ', ';
                // Check if image provided
                if (!empty(get_sub_field('author_image'))) {
                    $custom_author_image = get_sub_field('author_image');
                    $custom_author_image = $custom_author_image['sizes'];
                    $custom_author_image['thumbnail'];
                    $author_img_class = ( $custom_author_image['thumbnail-width'] < $custom_author_image['thumbnail-height'] ) ? 'portrait' : '';

                    $authors_image_output .= '<div class="img-circle">';
                    $authors_image_output .= '<img src="' . $custom_author_image['thumbnail'] . '" alt="' . $custom_author_name . '" class="' . $author_img_class . '">';
                    $authors_image_output .= '</div>'; 

                    $total_authors_array[] = $custom_author_image;
                }
            }
        endwhile;
    }

    $authors_name_output = substr($authors_name_output, 0, -2);
}

if (count($total_authors_array) > 1) {
    $multi_author_class = 'list-item__authors--multi';

    if (count($total_authors_array) === 3) {
        $multi_author_class .= ' list-item__authors--multi-3';
    }

    if (count($total_authors_array) === 4) {
        $multi_author_class .= ' list-item__authors--multi-4';
    }
}
@endphp

<article class="list-item list-item--post column xs-100 md-80 reveal">
    <div class="row">
        <div class="column xs-100 md-80">
            <div class="list-item__content">
                @if($categories_output)
                    <span class="list-item__label">{{ $categories_output }}</span>
                @endif  
                @if(has_post_thumbnail())
                    <a href="{{ get_permalink() }}" class="list-item__image-link image-zoom">
                        <div class="list-item__image img-cover">
                            {!! $featured_image !!}
                        </div>    
                    </a>
                @endif  
                <h2 class="list-item__title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
                <p class="list-item__excerpt">{!! strip_tags(App\visceral_get_the_excerpt(45)) !!}</p>
            </div>
        </div>

        <div class="column xs-100 md-20">
            <div class="list-item__meta">
                @if($authors)
                    <div class="list-item__authors {{ $multi_author_class }}">
                        @if($authors_image_output)
                            <div class="list-item__authors__images">
                                {!! $authors_image_output !!}
                            </div>
                        @endif  
                        @if($authors_name_output)  
                            <div class="list-item__authors__names">
                                <p>{{ $authors_name_output }}</p>
                            </div>   
                        @endif
                    </div>
                @endif
                <p class="list-item__date">{{ get_the_date() }}</p>
            </div>
        </div>
    </div>
</article>