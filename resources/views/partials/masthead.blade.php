@php
/**
 * Masthead
 *
 * This file has a lot going on
 *
 * First we select the right hero image to load
 * Then we load in the small version as a placeholder, and the large version lazy loaded in.
 */

// Set up variable
$wrapper_class			= '';
$parent_id				= '';
$image_id				= '';
$full_size_image_url	= '';
$placeholder_image_url	= '';
$breadcrumb				= '';
$title 					= '';
$sub_heading 			= '';

// Start off with a check for custom header images early and get the ID
if ( function_exists('get_field') && get_field( 'custom_header' ) != '' ) {
	$custom_header = get_field('custom_header');
	$image_id = $custom_header['id'];
}

// Some pages/posts/archives may be part of a section and pull the header from a "parent"
elseif ( is_singular('post') ) {
	// For these, we get the "parent" ID and get the image later on.
	$parent_id = get_page_by_path( 'blog' )->ID;
}
elseif ( is_category() ) {
	$parent_id = get_page_by_path( 'about/whats-new' )->ID;
}

// There are templates some that just need custom images.
elseif ( is_search() || is_404() ) {
	// For static headers, we just load the same image for main and placeholder.
	$full_size_image_url = $placeholder_image_url = get_template_directory_uri() . '/dist/images/header-custom.jpg';
}

// Finally, if nothing else, we check for a featured image and get it's ID
elseif ( has_post_thumbnail() ) {
	$image_id = get_post_thumbnail_id();
}

// Remember the parent ID's we got above? Let's adopt their main image and get the ID
if ( $parent_id != '' ) {
	if ( function_exists('get_field') && get_field( 'custom_header', $parent_id ) != '' ) {
		$custom_header = get_field('custom_header', $parent_id);
		$image_id = $custom_header['id'];
	} elseif ( has_post_thumbnail( $parent_id ) ) {
		$image_id = get_post_thumbnail_id( $parent_id );
	}
}

// By now, we should have an image ID to use. Let's use it to get some URLs.
if ( $image_id != '' ) {
	$wrapper_class   = 'masthead--background';
	$full_size_image = wp_get_attachment_image_src( $image_id,'full', true);
    $full_size_image_url = $full_size_image[0];
    
    $thumbnail_size = 'thumbnail';

    if (get_post_type() === 'client') {
        $thumbnail_size = 'full';
    }

	$placeholder_image = wp_get_attachment_image_src( $image_id, $thumbnail_size, true);
	$placeholder_image_url = $placeholder_image[0];
}

// Breadcrumbs and header text
if ( function_exists('get_field') && get_field( 'alternate_title' ) != '' ) {
	$breadcrumb = App::title();
	$title =  get_field( 'alternate_title' );
} elseif ( 'impact_story' == get_post_type() ) {
	$breadcrumb = '<a href="/our-work/impact-stories/" class="breadcrumbs__link">Impact Stories</a>';
	$title = App::title();
} else {
	global $post;
	if ( '' == $parent_id && !is_front_page() ) $parent_id = $post->post_parent;
	$breadcrumb = get_the_title($parent_id);
	$title = App::title();
}

if ( 'impact_story' == get_post_type() && function_exists('get_field') && get_field( 'client_name' ) != '' ) {
	$sub_heading = get_field('client_name');
} elseif ( function_exists('get_field') && get_field( 'sub_heading' ) != '' ) {
	$sub_heading = get_field('sub_heading');
}

if ( is_page('checkout') || is_page('cart') ) {
	$breadcrumb = '<a href="/products/" class="breadcrumbs__link">Products</a>';
}

/**
 * Markup from here on
 */
@endphp

<div class="masthead {{ $wrapper_class }}" data-image-src="{{ $full_size_image_url }}">
	<?php // The low quality placeholder
	if ( $placeholder_image_url != '') : ?>
		<span class="masthead__overlay img-bg" style="background-image: url({{$placeholder_image_url}});"></span>
	<?php endif; ?>

	<div class="container">
		<div class="row">
			<div class="column md-75">
				@if( !is_search() )
				<p class="masthead__breadcrumbs reveal">{!! $breadcrumb !!}</p>
				@endif
				<h1 class="masthead__title reveal">{!! $title !!}</h1>
				@if ( $sub_heading )
					<h2 class="masthead__subtitle reveal">{!! $sub_heading !!}</h2>
				@endif
			</div>
		</div>
	</div>
</div>