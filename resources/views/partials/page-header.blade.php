@php
if ( function_exists('get_field') && get_field( 'sub_heading' ) != '' ) {
	$sub_heading = get_field('sub_heading');
}
$title = App::title();

@endphp
<header class="page-header">
    <div class="container">
        <div class="row">
            <div class="column xs-100 md-75">
                <h1 class="entry-title reveal">{!! $title !!}</h1>
                @if($sub_heading)
                    <h2 class="masthead__subtitle reveal">{{ $sub_heading }}</h2>
                @endif
            </div>
        </div>
    </div>    
</header>
