<article @php(post_class('row single-person-content'))>
    <header class="column xs-100 md-67">
        <div class="single__header single-person-content__header">
            <div class="breadcrumbs">
                <a href="/about-us/our-team/" class="breadcrumbs__link">{{ __('Our Team', 'visceral') }}</a>
            </div>
            @if (has_post_thumbnail())
            <div class="img-circle swirl-img reveal reveal--delay">{{ the_post_thumbnail() }}</div>
            @endif
            <h1 class="entry-title reveal">{{ get_the_title() }}</h1>
            <h2 class="h5 entry-role reveal">{{ $role }}</h2>
            @if ($organization)
            <h3 class="h5 entry-organization reveal">{{ $organization }}</h2>
                @endif
                <?php $obfuscatedEmail = App\obfuscateEmail($email); ?>
                <script type="text/javascript">
                    document.write('<a class="entry-email reveal" href="mailto:' + '<?php echo $obfuscatedEmail; ?>'.replace(/\\x/g, '%') + '"><?php echo __("Email ", "visceral") . $first_name; ?></a>');
                </script>
        </div>
    </header>
    <main class="column xs-100 md-67">
        <div class="single-person-content__main reveal">
            {{ the_content() }}
        </div>

    </main>
    <sidebar class="column xs-100 md-33">
        <div class="single-person-content__sidebar reveal">
            @if ($insights)
            <div class="single-person-content__sidebar__section single-person-content__sidebar__section--post">
                <h4>Insights &amp; Perspectives</h4>
                <div class="related-content__main">
                    @for ($i = 1; $i <= count($insights); $i++) <p><a href="{{ get_permalink($insights[$i-1]->ID) }}">{{
                            $insights[$i-1]->post_title }}</a></p>
                        @if (count($insights) > 3 && $i == 3)
                        <p><a href="#" class="related-content__link">View All + </a></p>
                        <div class="related-content__more">
                            @endif
                            @endfor
                            @if (count($insights) > 3)
                        </div>
                        @endif
                </div>
            </div>
            @endif

            @if ($events)
            <div class="single-person-content__sidebar__section single-person-content__sidebar__section--event">
                <h4>Events</h4>
                <div class="related-content__main">
                    @for ($i = 1; $i <= count($events); $i++) <p><a href="{{ get_permalink($events[$i-1]->ID) }}">{{
                            $events[$i-1]->post_title }}</a></p>
                        @if (count($events) > 3 && $i == 3)
                        <p><a href="#" class="related-content__link">View All + </a></p>
                        <div class="related-content__more">
                            @endif
                            @endfor
                            @if (count($events) > 3)
                        </div>
                        @endif
                </div>
            </div>
            @endif

            @if ($resources)
            <div class="single-person-content__sidebar__section single-person-content__sidebar__section--resource">
                <h4>Resources</h4>
                <div class="related-content__main">
                    @for ($i = 1; $i <= count($resources); $i++) <p><a
                            href="{{ get_permalink($resources[$i-1]->ID) }}">{{ $resources[$i-1]->post_title }}</a></p>
                        @if (count($resources) > 3 && $i == 3)
                        <p><a href="#" class="related-content__link">View All + </a></p>
                        <div class="related-content__more">
                            @endif
                            @endfor
                            @if (count($resources) > 3)
                        </div>
                        @endif
                </div>
            </div>
            @endif
        </div>
    </sidebar>
</article>
<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author" => array(
				"@type" => "Person",
				"name" => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified" =>  get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>
<script type='application/ld+json'>
    <?php echo json_encode( $json_ld ); ?>
</script>
<?php endif; ?>