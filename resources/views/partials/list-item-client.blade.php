@php
$sectors = wp_get_post_terms(get_the_id(), 'sector', array('fields' => 'names') );
$sectors_output = '';

if (!empty($sectors)) {
    $sectors_output = $sectors[0];
}
@endphp

<article class="list-item-client column xs-100 md-50 lg-33 reveal">
    <p class="list-item-client__name">
        {{ get_the_title() }}
    </p>  
</article>