@php
$this_url = urlencode(get_permalink()); 
$this_post = get_post(get_the_id());
$this_title = $this_post->post_title;
$this_excerpt = urlencode ( get_the_excerpt() );
@endphp 

<div class="social-share">
	<ul class="list-inline">
		<li class="twitter">
			<a href="https://twitter.com/intent/tweet?source={{$this_url}}&amp;text={{$this_title}}': '{{$this_url}}" target="_blank" rel="noreferrer"><i class="icon icon-twitter" >{{ _e('Twitter', 'visceral')}}</i></a>
		</li>
		<li class="facebook">
			<a href="https://www.facebook.com/sharer/sharer.php?u={{$this_url}}&amp;t={{$this_title}}" target="_blank" rel="noreferrer"><i class="icon icon-facebook" >{{ _e('FAcebook', 'visceral')}}</i></a>
		</li>
		<li class="linkedin">
        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{$this_url}}&amp;title={{$this_title}}&amp;summary={{$this_excerpt}}" target="_blank" rel="noreferrer"><i class="icon icon-linkedin" >{{ _e('LinkedIn', 'visceral')}}</i></a>
		</li>
		<li class="email">
			<a href="mailto:?subject={{$this_title}}&body={{$this_title}}': '{{$this_url}}" target="_blank" rel="noreferrer"><i class="icon icon-mail" >{{ _e('Email', 'visceral')}}</i></a>
		</li>
	</ul>
</div>	