<section class="signup text-white text-center no-print">
    <div class="container">
        <div class="row justify-center">
            <div class="column xs-100 md-75 lg-50">
                <h6>{{ __('Stay Updated', 'visceral')}}</h6>
                <p class="h3">{{ __('Join our email list to stay updated with TCC Group’s practices, tools, and resources.', 'visceral')}}</p>
                <form action="/stay-connected">
                    <label for="email-signup" class="screen-reader-text">{{ __('Email', 'visceral')}}</label>
                    <input class="small" type="text" id="email-signup" name="email-signup" placeholder="Email Address">
                    <input class="btn btn--purple" type="submit" value="Sign Up">
                </form>
            </div>
        </div>
    </div>
</section>