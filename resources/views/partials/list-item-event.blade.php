@php
$event_types        = wp_get_post_terms(get_the_id(), 'event_type', array('fields' => 'names') );
$event_types_output = '';
$featured_image     = App\get_aspect_ratio_image(2, 1, 'large');
$excerpt            = wp_trim_words(get_the_excerpt(), 40 );

if (!empty($event_types)) {
    foreach ($event_types as $event_type) {
        $event_types_output .= $event_type . ', ';
    }

    $event_types_output = substr($event_types_output, 0, -2);
}

if (function_exists('get_field')) {
    $date_day = date_format(date_create(get_field('date')), 'j');
    $date_month = date_format(date_create(get_field('date')), 'M');
    $date_full = date_format(date_create(get_field('date')), 'F j, Y');
}

@endphp

<article class="list-item-event column xs-100 reveal">
    <div class="row">
        <div class="column md-20 event-date-container">
            <div class="event-date">
                <div class="event-date__inner">
                    <p>{{$date_month}}</p>
                    <p>{{$date_day}}</p>
                </div>
            </div>
        </div>
        <div class="column xs-100 md-80">
            @if(has_post_thumbnail())
                <a href="{{ get_permalink() }}" class="list-item-event__image image-zoom">
                    <div class="img-cover">
                        {!! $featured_image !!}
                    </div>
                </a>
            @endif  
            <h2 class="list-item-event__title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
            <p class="small">{!! $excerpt !!}</p>
            <div class="list-item-event__meta">
            <p class="event-date-full small">{{$date_full}}</p>
            <p class="small">{{$event_types_output}}</p>
            </div>
        </div>
    </div>
</article>