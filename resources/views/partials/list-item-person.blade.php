@php
$featured_image = App\get_aspect_ratio_image();
$person_role = get_post_meta(get_the_id(), 'role', true);
// $featured_image = get_the_post_thumbnail(get_the_id(), 'medium');
$management = has_term('management-team', 'department');
@endphp

<article class="list-item-person column xs-100 sm-50 lg-25 text-center reveal">
    <a href="{{ get_permalink() }}" class="">
        <div class="list-item-person__image img-circle">
            @if(has_post_thumbnail())
            {!! $featured_image !!}
            {{-- {!! get_the_post_thumbnail() !!} --}}
            @endif()
        </div>
        <h2 class="list-item-person__name">{{ get_the_title() }}</h2>
        <p class="list-item-person__role">{{ $person_role }}</p>
        @if ($management)
        <i class="list-item-person__role">{{ __('Management Team', 'visceral') }}</i>
        @endif
    </a>
</article>