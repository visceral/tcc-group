@php
$featured_image = App\get_aspect_ratio_image(2, 1);
@endphp

<article class="list-item-experience column xs-100 sm-50 xl-33 reveal">
    <a href="{{ get_permalink() }}" class="list-item-experience__link image-zoom">
        <div class="list-item-experience__image img-cover">
            @if(has_post_thumbnail())
                {!! $featured_image !!}
            @endif
        </div>
        <div class="list-item-experience__content">
            <h2 class="list-item-experience__title">{{ get_the_title() }}</h2>
            <p class="list-item-experience__excerpt">{{ get_the_excerpt() }}</p>
        </div>
    </a>
</article>