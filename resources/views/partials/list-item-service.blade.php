@php
$featured_image = App\get_aspect_ratio_image(1, 1, 'large');
@endphp

<article class="list-item-service column xs-100 sm-50 xl-33 reveal">
    <a href="{{ get_permalink() }}" class="list-item-service__link image-zoom">
        <div class="list-item-service__image img-cover">
            @if(has_post_thumbnail())
                {!! $featured_image !!}
            @endif
        </div>
        <div class="list-item-service__content">
            <h2 class="list-item-service__title">{{ get_the_title() }}</h2>
            <p class="list-item-service__excerpt">{{ wp_trim_words(get_the_excerpt(), 30 ) }}</p>
        </div>
    </a>
</article>