<header class="header container-fluid">
    <div class="header__inner">
        <a class="header__brand" href="{{ home_url('/') }}">
            <svg id="site-logo" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 312 88.33">
                <defs>
                    <style>
                        .cls-1 {
                            fill: none;
                        }

                        .cls-2 {
                            clip-path: url(#clip-path);
                        }

                        .cls-3 {
                            fill: #6f6f71;
                        }

                        .cls-4 {
                            clip-path: url(#clip-path-2);
                        }

                        .cls-5 {
                            fill: #3e99c0;
                        }

                        .cls-6 {
                            fill: #2f2e7c;
                        }

                        .cls-7 {
                            fill: #00769d;
                        }

                        .cls-8 {
                            fill: #7e6fab;
                        }

                        .cls-9 {
                            fill: #e08126;
                        }

                        .cls-10 {
                            fill: #d0532b;
                        }
                    </style>
                    <clipPath id="clip-path">
                        <rect class="cls-1" x="1.64" y="1.62" width="308.8" height="86.71" />
                    </clipPath>
                    <clipPath id="clip-path-2">
                        <rect class="cls-1" x="1.64" y="1.62" width="308.79" height="86.71" />
                    </clipPath>
                </defs>
                <title>tcc-logo</title>
                <g class="cls-2">
                    <path class="cls-3"
                        d="M39.76,75a3.44,3.44,0,0,0-1.53-1.2,5.1,5.1,0,0,0-2-.42,5.18,5.18,0,0,0-1.36.18,3.81,3.81,0,0,0-1.19.54,2.76,2.76,0,0,0-.83.93,2.66,2.66,0,0,0-.33,1.36,2.36,2.36,0,0,0,.26,1.15,2.43,2.43,0,0,0,.68.78,3.22,3.22,0,0,0,.92.48,10.26,10.26,0,0,0,1,.3,8.91,8.91,0,0,1,1.92.63,1.06,1.06,0,0,1,.65,1,1.31,1.31,0,0,1-.16.69,1.24,1.24,0,0,1-.45.43,2,2,0,0,1-.62.24,3.38,3.38,0,0,1-.72.07,3.07,3.07,0,0,1-1.52-.39,3.32,3.32,0,0,1-1.09-1L32,82a4.34,4.34,0,0,0,1.78,1.32,5.73,5.73,0,0,0,2.18.44,6.32,6.32,0,0,0,1.45-.17A3.58,3.58,0,0,0,38.63,83a2.77,2.77,0,0,0,.88-1,3,3,0,0,0,.33-1.47,2.32,2.32,0,0,0-.25-1.12,2.46,2.46,0,0,0-.69-.82,4,4,0,0,0-1-.57,12.52,12.52,0,0,0-1.3-.4,7,7,0,0,1-1.68-.54,1,1,0,0,1-.56-.93,1.1,1.1,0,0,1,.15-.59,1.21,1.21,0,0,1,.42-.39,1.82,1.82,0,0,1,.59-.23,3,3,0,0,1,.66-.07,2.66,2.66,0,0,1,1.35.34,2.54,2.54,0,0,1,.93.88ZM52,76.44a4.89,4.89,0,0,0-1.12-1.63,5.15,5.15,0,0,0-1.69-1.06,5.82,5.82,0,0,0-4.19,0,4.86,4.86,0,0,0-2.79,2.69,5.32,5.32,0,0,0,1.11,5.83A5.06,5.06,0,0,0,45,83.35a6,6,0,0,0,4.19,0,5.2,5.2,0,0,0,1.69-1.08A5.08,5.08,0,0,0,52,80.62a5.66,5.66,0,0,0,0-4.18M50.1,79.88A3.44,3.44,0,0,1,49.47,81a2.87,2.87,0,0,1-1,.78,3.55,3.55,0,0,1-2.81,0,3,3,0,0,1-1-.78A3.62,3.62,0,0,1,44,79.88a4.28,4.28,0,0,1-.22-1.34A4.34,4.34,0,0,1,44,77.19a3.32,3.32,0,0,1,.63-1.12,3,3,0,0,1,1-.78,3.55,3.55,0,0,1,2.81,0,2.87,2.87,0,0,1,1,.78,3.16,3.16,0,0,1,.63,1.12,4.07,4.07,0,0,1,.22,1.35,4,4,0,0,1-.22,1.34M57,67.79h-2V83.46h2ZM69.14,83.46c0-.32,0-.68-.07-1.1s0-.75,0-1V73.63h-2v5.23a3.6,3.6,0,0,1-.72,2.35,2.35,2.35,0,0,1-1.91.88,2.19,2.19,0,0,1-1.12-.25,1.73,1.73,0,0,1-.68-.66,2.89,2.89,0,0,1-.35-1,7.18,7.18,0,0,1-.09-1.16V73.63H60.28v6.1a5.53,5.53,0,0,0,.22,1.55,3.71,3.71,0,0,0,.66,1.28,3.14,3.14,0,0,0,1.15.86,3.93,3.93,0,0,0,1.64.31,3.69,3.69,0,0,0,2-.55,3.22,3.22,0,0,0,1.26-1.34h0c0,.21,0,.47,0,.78s0,.59.06.84Zm8.29-9.83H74.82V70.81H72.89v2.82H71v1.6h1.89v5.33a3.21,3.21,0,0,0,.74,2.3,2.8,2.8,0,0,0,2.12.77,5,5,0,0,0,.88-.08,5.24,5.24,0,0,0,.78-.19l-.06-1.6a2.44,2.44,0,0,1-.56.17,3.09,3.09,0,0,1-.58.06,1.31,1.31,0,0,1-1.06-.39,1.9,1.9,0,0,1-.33-1.26V75.23h2.61Zm4,0h-2v9.83h2Zm-.07-4.44a1.27,1.27,0,0,0-.92-.37,1.26,1.26,0,0,0-1.27,1.27,1.18,1.18,0,0,0,.37.88,1.22,1.22,0,0,0,.9.36,1.29,1.29,0,0,0,.92-.35,1.16,1.16,0,0,0,.38-.89,1.2,1.2,0,0,0-.38-.9m13,7.25a4.89,4.89,0,0,0-1.12-1.63,5.15,5.15,0,0,0-1.69-1.06,5.82,5.82,0,0,0-4.19,0,5,5,0,0,0-1.68,1.06,4.84,4.84,0,0,0-1.1,1.63,5.53,5.53,0,0,0,0,4.18,4.87,4.87,0,0,0,2.78,2.73,6,6,0,0,0,4.19,0,5.2,5.2,0,0,0,1.69-1.08,5.08,5.08,0,0,0,1.12-1.65,5.66,5.66,0,0,0,0-4.18m-1.85,3.44A3.44,3.44,0,0,1,91.89,81a3,3,0,0,1-1,.78,3.55,3.55,0,0,1-2.81,0A3,3,0,0,1,87,81a3.62,3.62,0,0,1-.63-1.14,4,4,0,0,1-.22-1.34,4.07,4.07,0,0,1,.22-1.35A3.32,3.32,0,0,1,87,76.07a3,3,0,0,1,1-.78,3.55,3.55,0,0,1,2.81,0,3,3,0,0,1,1,.78,3.16,3.16,0,0,1,.63,1.12,4.07,4.07,0,0,1,.22,1.35,4,4,0,0,1-.22,1.34m4.85-6.25c0,.32,0,.69.06,1.1s0,.76,0,1v7.71h2V78.23a3.56,3.56,0,0,1,.74-2.35A2.37,2.37,0,0,1,102,75a2.19,2.19,0,0,1,1.12.25,2,2,0,0,1,.69.66,2.88,2.88,0,0,1,.34,1,6.93,6.93,0,0,1,.09,1.14v5.45h2V77.37a5.21,5.21,0,0,0-.22-1.56,3.18,3.18,0,0,0-.67-1.26,3.43,3.43,0,0,0-1.14-.87,3.9,3.9,0,0,0-1.64-.32,3.52,3.52,0,0,0-1,.15,3.82,3.82,0,0,0-.92.4,4.33,4.33,0,0,0-.73.6,2.73,2.73,0,0,0-.51.74H99.3c0-.21,0-.47,0-.78s0-.59-.06-.84ZM116.21,75a3.44,3.44,0,0,0-1.53-1.2,5.1,5.1,0,0,0-2-.42,5.26,5.26,0,0,0-1.37.18,3.86,3.86,0,0,0-1.18.54,2.9,2.9,0,0,0-.84.93,2.76,2.76,0,0,0-.32,1.36,2.36,2.36,0,0,0,.26,1.15,2.53,2.53,0,0,0,.67.78,3.42,3.42,0,0,0,.93.48,10.26,10.26,0,0,0,1,.3,9.11,9.11,0,0,1,1.92.63,1.06,1.06,0,0,1,.65,1,1.31,1.31,0,0,1-.16.69,1.24,1.24,0,0,1-.45.43,2,2,0,0,1-.62.24,3.35,3.35,0,0,1-.71.07,3.08,3.08,0,0,1-1.53-.39,3.32,3.32,0,0,1-1.09-1L108.41,82a4.27,4.27,0,0,0,1.78,1.32,5.73,5.73,0,0,0,2.18.44,6.32,6.32,0,0,0,1.45-.17,3.58,3.58,0,0,0,1.26-.55,2.87,2.87,0,0,0,.88-1,3,3,0,0,0,.33-1.47,2.32,2.32,0,0,0-.25-1.12,2.35,2.35,0,0,0-.69-.82,4,4,0,0,0-1-.57,12.52,12.52,0,0,0-1.3-.4,7,7,0,0,1-1.68-.54,1,1,0,0,1-.56-.93,1.1,1.1,0,0,1,.15-.59,1.21,1.21,0,0,1,.42-.39,1.82,1.82,0,0,1,.59-.23,3,3,0,0,1,.66-.07,2.66,2.66,0,0,1,1.35.34,2.54,2.54,0,0,1,.93.88Zm12.56-1.35h-2.28v-2a6.19,6.19,0,0,1,.06-.88,2.52,2.52,0,0,1,.25-.76,1.42,1.42,0,0,1,.53-.53,1.82,1.82,0,0,1,.9-.19,2.75,2.75,0,0,1,.52,0l.46.1.22-1.68a4.82,4.82,0,0,0-1.22-.14,4.11,4.11,0,0,0-1.69.31,3.16,3.16,0,0,0-1.14.85,3.35,3.35,0,0,0-.64,1.28,6.15,6.15,0,0,0-.2,1.6v2h-2v1.6h2v8.23h1.95V75.23h2.28Zm11.42,2.81a4.92,4.92,0,0,0-1.13-1.63,5,5,0,0,0-1.68-1.06,5.82,5.82,0,0,0-4.19,0,4.86,4.86,0,0,0-2.79,2.69,5.32,5.32,0,0,0,1.11,5.83,5.06,5.06,0,0,0,1.68,1.08,6,6,0,0,0,4.19,0,5,5,0,0,0,2.81-2.73,5.66,5.66,0,0,0,0-4.18m-1.85,3.44A3.65,3.65,0,0,1,137.7,81a2.84,2.84,0,0,1-1,.78,3.32,3.32,0,0,1-1.41.29,3.27,3.27,0,0,1-1.4-.29,2.87,2.87,0,0,1-1-.78,3.44,3.44,0,0,1-.63-1.14,4,4,0,0,1-.22-1.34,4.07,4.07,0,0,1,.22-1.35,3.16,3.16,0,0,1,.63-1.12,2.87,2.87,0,0,1,1-.78,3.27,3.27,0,0,1,1.4-.29,3.32,3.32,0,0,1,1.41.29,2.84,2.84,0,0,1,1,.78,3.34,3.34,0,0,1,.64,1.12,4.34,4.34,0,0,1,.22,1.35,4.28,4.28,0,0,1-.22,1.34m4.93-4.13v7.71h1.95V78.3a4.17,4.17,0,0,1,.16-1.19,2.83,2.83,0,0,1,.51-1,2.68,2.68,0,0,1,.86-.7,2.77,2.77,0,0,1,1.22-.25,3.71,3.71,0,0,1,.9.1l.08-1.8a2.86,2.86,0,0,0-.77-.09,3.19,3.19,0,0,0-1.79.53,3.35,3.35,0,0,0-1.21,1.4h-.07c0-.21,0-.47,0-.8s0-.61-.06-.86h-1.84c0,.32,0,.69.06,1.1s0,.76,0,1M162.82,75a3.44,3.44,0,0,0-1.53-1.2,5.1,5.1,0,0,0-2-.42,5.2,5.2,0,0,0-1.37.18,3.86,3.86,0,0,0-1.18.54,2.66,2.66,0,0,0-1.16,2.29,2.36,2.36,0,0,0,.26,1.15,2.53,2.53,0,0,0,.67.78,3.51,3.51,0,0,0,.92.48c.35.12.68.22,1,.3a9.11,9.11,0,0,1,1.92.63,1.06,1.06,0,0,1,.65,1,1.22,1.22,0,0,1-.17.69,1.14,1.14,0,0,1-.44.43,2,2,0,0,1-.62.24,3.38,3.38,0,0,1-.72.07,3,3,0,0,1-1.52-.39,3.32,3.32,0,0,1-1.09-1L155,82a4.27,4.27,0,0,0,1.78,1.32,5.7,5.7,0,0,0,2.18.44,6.32,6.32,0,0,0,1.45-.17,3.58,3.58,0,0,0,1.26-.55,2.87,2.87,0,0,0,.88-1,3,3,0,0,0,.33-1.47,2.32,2.32,0,0,0-.25-1.12,2.58,2.58,0,0,0-.69-.82,4,4,0,0,0-1.05-.57,12.72,12.72,0,0,0-1.31-.4,7.36,7.36,0,0,1-1.68-.54,1,1,0,0,1-.55-.93,1.1,1.1,0,0,1,.15-.59,1.18,1.18,0,0,1,.41-.39,2,2,0,0,1,.6-.23,3,3,0,0,1,.66-.07,2.66,2.66,0,0,1,1.35.34,2.54,2.54,0,0,1,.93.88ZM175,76.44a5,5,0,0,0-1.12-1.63,5.15,5.15,0,0,0-1.69-1.06,5.82,5.82,0,0,0-4.19,0,4.86,4.86,0,0,0-2.79,2.69,5.32,5.32,0,0,0,1.11,5.83A5.06,5.06,0,0,0,168,83.35a6,6,0,0,0,4.19,0A5,5,0,0,0,175,80.62a5.66,5.66,0,0,0,0-4.18m-1.85,3.44a3.44,3.44,0,0,1-.63,1.14,3,3,0,0,1-1,.78,3.55,3.55,0,0,1-2.81,0,3,3,0,0,1-1-.78,3.44,3.44,0,0,1-.63-1.14,4,4,0,0,1-.22-1.34,4.07,4.07,0,0,1,.22-1.35,3.16,3.16,0,0,1,.63-1.12,3,3,0,0,1,1-.78,3.55,3.55,0,0,1,2.81,0,3,3,0,0,1,1,.78,3.16,3.16,0,0,1,.63,1.12,4.07,4.07,0,0,1,.22,1.35,4,4,0,0,1-.22,1.34M186.45,75a3.8,3.8,0,0,0-1.63-1.2,5.38,5.38,0,0,0-2.06-.42,5.69,5.69,0,0,0-2.08.38A4.91,4.91,0,0,0,179,74.8a4.74,4.74,0,0,0-1.1,1.63,5.54,5.54,0,0,0-.39,2.11,5.44,5.44,0,0,0,.4,2.14A4.81,4.81,0,0,0,179,82.31a5,5,0,0,0,1.67,1,5.94,5.94,0,0,0,2.1.37,5.27,5.27,0,0,0,2.12-.41,4.12,4.12,0,0,0,1.55-1.13L185.17,81a2.83,2.83,0,0,1-1,.79,3.19,3.19,0,0,1-1.36.29,3.12,3.12,0,0,1-1.36-.29,2.93,2.93,0,0,1-1-.78,3.21,3.21,0,0,1-.63-1.13,4,4,0,0,1-.22-1.35,4,4,0,0,1,.22-1.34,3.21,3.21,0,0,1,.63-1.13,3.11,3.11,0,0,1,1-.78,3,3,0,0,1,1.35-.29,2.9,2.9,0,0,1,1.33.32,2.46,2.46,0,0,1,.93.8Zm4-1.35h-1.95v9.83h1.95Zm-.07-4.44a1.27,1.27,0,0,0-.92-.37,1.26,1.26,0,0,0-1.27,1.27,1.18,1.18,0,0,0,.37.88,1.22,1.22,0,0,0,.9.36,1.29,1.29,0,0,0,.92-.35,1.16,1.16,0,0,0,.38-.89,1.2,1.2,0,0,0-.38-.9m9.52,8.18c-.82,0-1.65,0-2.46.1a9.31,9.31,0,0,0-2.2.44,3.86,3.86,0,0,0-1.58,1,2.51,2.51,0,0,0-.6,1.77,2.77,2.77,0,0,0,1.21,2.37,3.5,3.5,0,0,0,1.15.5,4.74,4.74,0,0,0,1.21.16,4.08,4.08,0,0,0,1.94-.44A3.76,3.76,0,0,0,200,82.05H200a6.16,6.16,0,0,0,.16,1.41H202a4.92,4.92,0,0,1-.12-.9q0-.56,0-1V77.22a4.49,4.49,0,0,0-.25-1.52,3.42,3.42,0,0,0-.75-1.22,3.62,3.62,0,0,0-1.27-.82,5.09,5.09,0,0,0-1.83-.3,6.16,6.16,0,0,0-2.25.4,5.21,5.21,0,0,0-1.78,1.08l1,1.24a3.89,3.89,0,0,1,1.23-.82,3.77,3.77,0,0,1,1.57-.32,2.75,2.75,0,0,1,1.76.53,2,2,0,0,1,.66,1.63Zm0,1.4v.52a3,3,0,0,1-.73,2.06,2.67,2.67,0,0,1-2.11.82,3.45,3.45,0,0,1-.72-.07,1.73,1.73,0,0,1-.64-.26,1.41,1.41,0,0,1-.47-.47,1.39,1.39,0,0,1-.18-.73,1.27,1.27,0,0,1,.42-1,2.76,2.76,0,0,1,1.05-.56,7.13,7.13,0,0,1,1.41-.24c.51,0,1-.07,1.5-.07Zm7-11H205V83.46H207Zm10.76,5.84h-2v9.83h2Zm-.07-4.44a1.25,1.25,0,0,0-.91-.37,1.26,1.26,0,0,0-1.28,1.27,1.22,1.22,0,0,0,.37.88,1.34,1.34,0,0,0,1.82,0,1.13,1.13,0,0,0,.38-.89,1.17,1.17,0,0,0-.38-.9M223,75.25h-.07c0-.21,0-.47,0-.78s0-.59-.06-.84H221c0,.32,0,.69.06,1.1s0,.76,0,1v7.71H223V78.23a3.6,3.6,0,0,1,.71-2.35,2.25,2.25,0,0,1,1.8-.88,2.18,2.18,0,0,1,1,.2,1.68,1.68,0,0,1,.64.54,2.27,2.27,0,0,1,.36.82,5.59,5.59,0,0,1,.1,1v5.85h1.95V78.15A4.86,4.86,0,0,1,229.7,77a3,3,0,0,1,.44-1,2.37,2.37,0,0,1,.78-.73,2.13,2.13,0,0,1,1.13-.28,2,2,0,0,1,1.06.25,1.9,1.9,0,0,1,.66.66,2.9,2.9,0,0,1,.36,1,6,6,0,0,1,.1,1.14v5.45h2V77.37a5.21,5.21,0,0,0-.22-1.56,3.32,3.32,0,0,0-.67-1.26,3.43,3.43,0,0,0-1.14-.87,4.15,4.15,0,0,0-3.59.19,3.56,3.56,0,0,0-1.39,1.46,3,3,0,0,0-1.25-1.51,3.58,3.58,0,0,0-1.79-.46,3.43,3.43,0,0,0-1.94.55A3.23,3.23,0,0,0,223,75.25M241.38,82h.06a3.59,3.59,0,0,0,1.47,1.28,4.36,4.36,0,0,0,2,.46,5,5,0,0,0,2.05-.41,4.55,4.55,0,0,0,1.55-1.1,5,5,0,0,0,1-1.66,5.75,5.75,0,0,0,.36-2,5.67,5.67,0,0,0-.36-2,4.89,4.89,0,0,0-1-1.64,4.52,4.52,0,0,0-1.55-1.1,4.83,4.83,0,0,0-2-.4,4.37,4.37,0,0,0-2.14.51,3.57,3.57,0,0,0-1.39,1.3h-.06V73.63h-1.91v14.8h1.95Zm6.26-2.14A3.5,3.5,0,0,1,247,81a3,3,0,0,1-1,.8,3.42,3.42,0,0,1-2.78,0,3.24,3.24,0,0,1-1.05-.78,3.64,3.64,0,0,1-.67-1.13,3.76,3.76,0,0,1-.24-1.35,3.81,3.81,0,0,1,.91-2.48,3.42,3.42,0,0,1,1.05-.79,3.19,3.19,0,0,1,1.36-.29,3.33,3.33,0,0,1,1.42.29,3.12,3.12,0,0,1,1,.79,3.5,3.5,0,0,1,.62,1.14,4.49,4.49,0,0,1,0,2.65m11.05-2.48c-.83,0-1.66,0-2.47.1a9.31,9.31,0,0,0-2.2.44,3.76,3.76,0,0,0-1.57,1,2.51,2.51,0,0,0-.6,1.77,2.75,2.75,0,0,0,.34,1.43,2.89,2.89,0,0,0,.86.94,3.59,3.59,0,0,0,1.15.5,4.8,4.8,0,0,0,1.21.16,4.08,4.08,0,0,0,1.94-.44,3.79,3.79,0,0,0,1.36-1.22h.06a6.18,6.18,0,0,0,.17,1.41h1.76a5,5,0,0,1-.11-.9c0-.37,0-.72,0-1V77.22a4.49,4.49,0,0,0-.25-1.52,3.39,3.39,0,0,0-.74-1.22,3.67,3.67,0,0,0-1.28-.82,5,5,0,0,0-1.83-.3,6.2,6.2,0,0,0-2.25.4,5.07,5.07,0,0,0-1.77,1.08l1,1.24a4.06,4.06,0,0,1,1.24-.82,3.76,3.76,0,0,1,1.56-.32,2.8,2.8,0,0,1,1.77.53,2,2,0,0,1,.66,1.63Zm0,1.4v.52a3,3,0,0,1-.74,2.06,2.66,2.66,0,0,1-2.1.82,3.54,3.54,0,0,1-.73-.07,1.8,1.8,0,0,1-.64-.26,1.51,1.51,0,0,1-.47-.47,1.49,1.49,0,0,1-.17-.73,1.3,1.3,0,0,1,.41-1,2.92,2.92,0,0,1,1.05-.56,7.13,7.13,0,0,1,1.41-.24c.52,0,1-.07,1.5-.07ZM272.08,75a3.8,3.8,0,0,0-1.63-1.2,5.38,5.38,0,0,0-2.06-.42,5.66,5.66,0,0,0-2.08.38,4.81,4.81,0,0,0-1.67,1.06,4.74,4.74,0,0,0-1.1,1.63,5.55,5.55,0,0,0-.4,2.11,5.44,5.44,0,0,0,.41,2.14,4.81,4.81,0,0,0,1.11,1.63,4.94,4.94,0,0,0,1.67,1,5.94,5.94,0,0,0,2.1.37,5.22,5.22,0,0,0,2.11-.41,4.08,4.08,0,0,0,1.56-1.13L270.79,81a2.71,2.71,0,0,1-1,.79,3.36,3.36,0,0,1-2.73,0,2.89,2.89,0,0,1-1-.78,3.39,3.39,0,0,1-.64-1.13,4.24,4.24,0,0,1-.21-1.35,4.32,4.32,0,0,1,.21-1.34,3.39,3.39,0,0,1,.64-1.13A3,3,0,0,1,268.41,75a2.93,2.93,0,0,1,1.33.32,2.46,2.46,0,0,1,.93.8Zm7.68-1.35h-2.61V70.81h-1.93v2.82h-1.89v1.6h1.89v5.33a3.21,3.21,0,0,0,.74,2.3,2.81,2.81,0,0,0,2.12.77,5.69,5.69,0,0,0,1.66-.27l-.06-1.6a2.44,2.44,0,0,1-.56.17,3.09,3.09,0,0,1-.58.06,1.31,1.31,0,0,1-1.06-.39,1.9,1.9,0,0,1-.33-1.26V75.23h2.61Z" />
                </g>
                <g class="cls-4">
                    <path class="cls-5"
                        d="M152.87,18.17A19,19,0,0,0,146.42,14a23.61,23.61,0,0,0-8.84-1.52,22.74,22.74,0,0,0-8.76,1.67A21.07,21.07,0,0,0,117.2,25.75a23.29,23.29,0,0,0-1.67,8.91,23.61,23.61,0,0,0,1.64,8.9,21.16,21.16,0,0,0,4.57,7,20.68,20.68,0,0,0,7,4.63,22.94,22.94,0,0,0,8.85,1.68,33.52,33.52,0,0,0,8.3-1,27.35,27.35,0,0,0,7.47-3.12l-.28-20.4H137.56V36.1h11.49V50.31a16.77,16.77,0,0,1-4.84,2,26.44,26.44,0,0,1-6.57.77,17.29,17.29,0,0,1-7.32-1.49,16.74,16.74,0,0,1-5.53-4,17.7,17.7,0,0,1-3.5-5.85,20.92,20.92,0,0,1,0-14.08,18,18,0,0,1,3.5-5.88,16.4,16.4,0,0,1,5.53-4,17.29,17.29,0,0,1,7.32-1.49,17.82,17.82,0,0,1,7,1.28,15.58,15.58,0,0,1,5.26,3.61Z" />
                    <path class="cls-5"
                        d="M166.21,32.39V17.27h7.64a21,21,0,0,1,4.36.42A9.7,9.7,0,0,1,181.56,19a6.49,6.49,0,0,1,2.15,2.33,7.34,7.34,0,0,1,.78,3.53,6.36,6.36,0,0,1-2.81,5.65q-2.81,1.88-8,1.88ZM178,35.73a14.83,14.83,0,0,0,4.3-1.07,11,11,0,0,0,3.4-2.24A10.26,10.26,0,0,0,188,29.1a10.42,10.42,0,0,0,.84-4.24,11.66,11.66,0,0,0-1.08-5.26,9.19,9.19,0,0,0-3-3.5,13.2,13.2,0,0,0-4.6-1.94,26.91,26.91,0,0,0-5.77-.59H161.9v42.3h4.31V36.09h7.17l11.71,19.78h5.26Z" />
                </g>
                <g class="cls-2">
                    <path class="cls-5"
                        d="M268.23,55.17A13.77,13.77,0,0,0,273.1,51a15,15,0,0,0,2.48-5.47,24.51,24.51,0,0,0,.69-5.58V13.43H272V39.48a20.66,20.66,0,0,1-.6,5,12.58,12.58,0,0,1-1.94,4.3,10,10,0,0,1-3.56,3,13.31,13.31,0,0,1-10.87,0,10,10,0,0,1-3.56-3,12.58,12.58,0,0,1-1.94-4.3,20.66,20.66,0,0,1-.6-5v-26h-4.3V40a24.51,24.51,0,0,0,.69,5.58A15.14,15.14,0,0,0,247.77,51a13.92,13.92,0,0,0,4.84,4.16,19.42,19.42,0,0,0,15.62,0" />
                    <path class="cls-5"
                        d="M303.74,30.59q-2.64,2-7.71,2h-7.29V17.27H296a13.67,13.67,0,0,1,7.62,1.82,6.46,6.46,0,0,1,2.72,5.77,6.72,6.72,0,0,1-2.63,5.73m5.91-10.72a9.26,9.26,0,0,0-2.9-3.52,12.79,12.79,0,0,0-4.51-2.1,23.49,23.49,0,0,0-5.85-.68H284.44v42.3h4.3V36.21h7.53a22.44,22.44,0,0,0,5.88-.72,13,13,0,0,0,4.54-2.12,9.52,9.52,0,0,0,2.93-3.52,11.15,11.15,0,0,0,1.05-5,11.4,11.4,0,0,0-1-5" />
                    <path class="cls-6"
                        d="M28.73,13.55H17.52V1.46H9.26V13.55H1.17V20.4H9.26V43.24q0,6.59,3.15,9.87t9.11,3.29a23.14,23.14,0,0,0,3.78-.31,21.15,21.15,0,0,0,3.34-.84l-.27-6.85a9.78,9.78,0,0,1-2.4.73,13.29,13.29,0,0,1-2.49.25,5.72,5.72,0,0,1-4.54-1.64c-.94-1.09-1.42-2.91-1.42-5.45V20.4H28.73ZM70.8,19.33a16.26,16.26,0,0,0-7-5.16A23.52,23.52,0,0,0,55,12.4,24.46,24.46,0,0,0,46,14,20.5,20.5,0,0,0,34.17,25.55a23.67,23.67,0,0,0-1.69,9.07,23.19,23.19,0,0,0,1.74,9.16,20.64,20.64,0,0,0,4.75,7,21.45,21.45,0,0,0,7.16,4.5,24.93,24.93,0,0,0,9,1.6,22.54,22.54,0,0,0,9.07-1.74,18.18,18.18,0,0,0,6.67-4.84L65.28,45.2A11.88,11.88,0,0,1,61,48.58a14.37,14.37,0,0,1-11.68,0,12.92,12.92,0,0,1-4.36-3.34,14.53,14.53,0,0,1-2.71-4.84,18.43,18.43,0,0,1,0-11.56A14.43,14.43,0,0,1,44.93,24a13.27,13.27,0,0,1,4.31-3.34,13,13,0,0,1,5.82-1.24,12.17,12.17,0,0,1,5.69,1.38,10.26,10.26,0,0,1,4,3.42Zm41.61,0a16.26,16.26,0,0,0-7-5.16,23.52,23.52,0,0,0-8.85-1.77A24.46,24.46,0,0,0,87.65,14a21.16,21.16,0,0,0-7.16,4.53,20.85,20.85,0,0,0-4.71,7,23.67,23.67,0,0,0-1.68,9.07,23.19,23.19,0,0,0,1.73,9.16,20.64,20.64,0,0,0,4.75,7,21.45,21.45,0,0,0,7.16,4.5,24.89,24.89,0,0,0,9,1.6,22.54,22.54,0,0,0,9.07-1.74,18.1,18.1,0,0,0,6.67-4.84l-5.6-5.07a12,12,0,0,1-4.32,3.38,14.4,14.4,0,0,1-11.69,0,13.08,13.08,0,0,1-4.35-3.34,14.53,14.53,0,0,1-2.71-4.84,18.43,18.43,0,0,1,0-11.56A14.43,14.43,0,0,1,86.54,24a13.37,13.37,0,0,1,4.31-3.34,13,13,0,0,1,5.82-1.24,12.17,12.17,0,0,1,5.69,1.38,10.26,10.26,0,0,1,4,3.42Z" />
                </g>
                <g class="cls-4">
                    <path class="cls-5"
                        d="M201.91,25.87a20.65,20.65,0,0,1,35.21-1,22.25,22.25,0,1,0-18.46,32,20.65,20.65,0,0,1-16.75-31" />
                    <path class="cls-7"
                        d="M199.84,24.79a21.27,21.27,0,0,1,7.81-7.89,20.75,20.75,0,0,1,8.55-2.77,22.61,22.61,0,0,1,4.93.06,25.9,25.9,0,0,1,4.16.93,21.77,21.77,0,0,1,6.32,3.39,21,21,0,0,1,3,2.82c.49.61,1,1.2,1.43,1.83.18.26.36.51.52.77l.29.47.21.35a.41.41,0,0,0,0,.09s0,0,0,0a22.25,22.25,0,1,0-18.46,32h-.83c-.23,0-.56,0-.93-.05a18.4,18.4,0,0,1-1.87-.22,26.64,26.64,0,0,1-4.13-1.1,17.2,17.2,0,0,1-3.51-1.72,25,25,0,0,1-3.33-2.49,21.43,21.43,0,0,1-7-16.07,21,21,0,0,1,2.44-9.7c.12-.24.25-.47.38-.7" />
                    <path class="cls-8"
                        d="M208.23,44.42A16.88,16.88,0,0,1,219.66,18,18.18,18.18,0,1,0,237,42.71a16.87,16.87,0,0,1-28.73,1.71" />
                    <path class="cls-6"
                        d="M206.71,45.58a17.4,17.4,0,0,1-3.14-8.51,17,17,0,0,1,.9-7.29,18.89,18.89,0,0,1,1.74-3.63,22.12,22.12,0,0,1,2.13-2.76A18.09,18.09,0,0,1,213,19.88a17.84,17.84,0,0,1,3.13-1.24c.62-.15,1.23-.32,1.85-.43l.74-.11.45-.06.33,0h.13A18.18,18.18,0,1,0,237,42.71h0s-.06.14-.07.15l-.11.23c-.05.11-.11.21-.12.24s-.21.41-.35.66c-.24.44-.55.93-.81,1.32A22.87,22.87,0,0,1,233.25,48a14.24,14.24,0,0,1-2.48,2,20.78,20.78,0,0,1-3,1.61,17.59,17.59,0,0,1-14.34-.34,17.18,17.18,0,0,1-6.34-5.15l-.39-.53" />
                    <path class="cls-9"
                        d="M225.18,45.49a13.49,13.49,0,0,1-16.73-15.82,14.54,14.54,0,1,0,23.31-6.24,13.25,13.25,0,0,1,3,5.5,13.51,13.51,0,0,1-9.56,16.56" />
                    <path class="cls-10"
                        d="M225.64,47a13.86,13.86,0,0,1-7.26,0,13.68,13.68,0,0,1-5.23-2.67A14.41,14.41,0,0,1,210.9,42a17.19,17.19,0,0,1-1.5-2.36A14.06,14.06,0,0,1,208,32.48c.06-.51.1-1,.19-1.51,0-.2.07-.4.11-.59s.06-.24.08-.35a2.15,2.15,0,0,0,.07-.26.64.64,0,0,1,0-.07v0a14.54,14.54,0,1,0,23.31-6.24h0l.09.09.14.14.15.16c.1.11.25.27.4.45s.55.66.77,1a18.69,18.69,0,0,1,1.4,2.42,11.45,11.45,0,0,1,.83,2.42,17.37,17.37,0,0,1,.39,2.69,14.12,14.12,0,0,1-1.29,6.58,14,14,0,0,1-2.89,4.1,13.8,13.8,0,0,1-5.61,3.36l-.5.15" />
                </g>
            </svg>
        </a>
        <nav class="header__navigation">
            @if (has_nav_menu('primary_menu'))
            {!! wp_nav_menu(['theme_location' => 'primary_menu', 'menu_class' => 'nav list-inline', 'walker' => new
            App\visc_walker]) !!}
            @endif
            <a class="icon-search header__search-button icon " href="{{ site_url() }}/?s="
                title="@php( _e('Search', 'visceral') )" aria-label="Search"><?php _e('Search', 'visceral'); ?></a>
        </nav>

        <label class="mobile-nav__icon no-print" id="mobile-nav-icon" for="nav-toggle" tabindex="0">
            <span class="screen-reader-text"><?php _e('Navigation Toggle', 'visceral'); ?></span>
            <div>
                <div class="mobile-nav__menu-line"></div>
                <div class="mobile-nav__menu-line"></div>
                <div class="mobile-nav__menu-line"></div>
                <div class="mobile-nav__menu-line"></div>
            </div>
        </label>
    </div>
    <div class="header__search">
        <div class="header__search-inner">
            <form role="search" class="header__search-form searchform" method="get" action="{{ site_url() }}">
                <label><span class="screen-reader-text"><?php _e('Search', 'visceral'); ?></span>
                    <input type="text" placeholder="<?php _e('Search', 'visceral'); ?>..." name="s" autocomplete="off"
                        spellcheck="false" autofocus>
                </label>
                <input type="submit" class="header__search-submit" value="<?php _e('Submit', 'visceral'); ?>">
            </form>
            <span class="header__search-close">
                <i class="icon icon-close" aria-label="Close"><?php _e('Close', 'visceral'); ?></i>
            </span>
        </div>
    </div>
</header>

<input type="checkbox" id="nav-toggle">

<?php // Responsive-nav ?>
<div id="mobile-nav">
    <div class="mobile-nav container no-print">
        <div class="row justify-center">
            <div>
                <?php // if( wp_is_mobile() ) { // display search, quicklinks, etc. ?>
                <form class="mobile-search" role="search" method="get" action="<?php echo site_url() ?>">
                    <label class="mobile-search__input" for="header-search"><span
                            class="screen-reader-text"><?php _e('Search', 'sage'); ?></span>
                        <input type="text" placeholder="<?php _e('Search', 'sage'); ?>..." name="s" id="header-search"
                            autocomplete="off" spellcheck="false" autofocus>
                    </label>
                    <button type="submit" class="mobile-search__submit">
                        <span class="icon icon-search">Search</span>
                    </button>
                </form>
                <?php // } ?>

                <?php
                $mobile_menu = 'primary_menu';
                if (has_nav_menu('mobile_menu')) {
                    $mobile_menu = 'mobile_menu';
                }
                if (has_nav_menu($mobile_menu)) :
					wp_nav_menu(['theme_location' => $mobile_menu, 'container_id' => '', 'container_class' => '', 'after' => '<span class="icon-chevron-right sub-menu--open"></span>', 'menu_class' => 'mobile-nav__menu', 'walker' => new App\visc_walker_mobile]);
				endif;
				?>
            </div>
        </div>
    </div>
</div>