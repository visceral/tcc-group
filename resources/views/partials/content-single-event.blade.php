
<article @php(post_class('single-event-content'))>
    <div class="row justify-center">
        <div class="column md-80">
            <header class="single__header single-event-content__header">
                @include('partials.single-social-share')
                <div class="breadcrumbs reveal">
					<a href="/events/" class="breadcrumbs__link">Events</a> {!! $get_main_event_type !!}
				</div>
                <h1 class="entry-title reveal">{{ get_the_title() }}</h1>
                
                <div class="single-event-content__meta row reveal">
                    <div class="column xs-100 md-50">
						@if( $event_date )
						<p class="h5 single-event-date">{{ $event_date }}</p>
						@endif
						@if( $event_time )
						<p class="h5 single-event-time">{{ $event_time }}</p>
						@endif
					</div>
					@if( $event_location )
                    <div class="column xs-100 md-50">
                        <p class="h5 single-event-location">{{ $event_location }}</p>
					</div>
					@endif
                </div>
            </header>
            <main class="single-event-content__main reveal">
				@if (has_post_thumbnail())
					<div class="single-event-content__main-image">{{ the_post_thumbnail() }}</div>
				@endif
                @php(the_content())
            </main>
            <div class="single-event-content__button-container reveal">
                @if($event_button_url)
                    <a href="{{ $event_button_url }}" class="btn">{{ $event_button_text }}</a>       
                @endif
            </div>
        </div> 
    </div>       
</article>

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author" => array(
				"@type" => "Person",
				"name" => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified" =>  get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>
		<script type='application/ld+json'>
			<?php echo json_encode( $json_ld ); ?>
		</script>
	<?php endif; ?>
