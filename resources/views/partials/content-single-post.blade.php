<article @php(post_class('single-post-content'))>
    <div class="row justify-center">
        <div class="column md-80">
            <header class="single__header">
                @include('partials.single-social-share')
				<div class="breadcrumbs reveal">
					<a href="/insights-resources/insights-perspectives/" class="breadcrumbs__link">Insights &amp; Perspectives</a> {!! $get_main_category !!}
				</div>
                <h1 class="entry-title reveal">{!! get_the_title() !!}</h1>
                @if($authors)
                    <div class="authors row reveal">
                        @if($authors)
							@foreach($authors as $author)
								@if($author->published)
									<a href="{{get_permalink($author->ID)}}" class="authors__author column xs-100 md-50">
										{!! $author->image !!}
										<p>{{ $author->name }}@if($author->role), {{ $author->role }}@endif</p>
									</a>
								@else
									<div href="{{get_permalink($author->ID)}}" class="authors__author column xs-100 md-50">
										{!! $author->image !!}
										<p>{{ $author->name }}@if($author->role), {{ $author->role }}@endif</p>
									</div>
								@endif
                            @endforeach
                        @endif
                    </div>
                @endif
            </header>
            <div class="entry-content reveal">       
                @php(the_content())
            </div>
            <div class="entry-meta reveal">
                <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
            </div>
        </div>
    </div>   
</article>  

@includeWhen($related_content, 'partials.related-content')

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author" => array(
				"@type" => "Person",
				"name" => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified" =>  get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>
		<script type='application/ld+json'>
			<?php echo json_encode( $json_ld ); ?>
		</script>
	<?php endif; ?>
