<section class="related-content">
    <p class="h6 text-center reveal related-content__heading">Learn More</p>
    <div class="row">
    @foreach ($related_content as $post)
        @php(setup_postdata($GLOBALS['post'] = $post)) 
                        
        @include('partials.list-item-card')
        @php(wp_reset_postdata())
    @endforeach
    </div>
    <p class="related-content__all-links text-center reveal">View all: <a href="/insights-resources/resource-library/" class="btn btn--small btn--magenta">Resources</a> <a href="/insights-resources/insights-perspectives/" class="btn btn--small btn--orange">Insights &amp; Perspectives</a> <a href="/our-work/impact-stories/" class="btn btn--small btn--blue">Impact Stories</a> <a href="/events/" class="btn btn--small btn--purple">Events</a></p>
</section>