<article @php(post_class('single-resource-content'))>
    <div class="row justify-center">
        <div class="column md-80">
            <header class="single__header single-resource-content__header">
                @include('partials.single-social-share')
                <div class="breadcrumbs">
                    <a href="/insights-resources/resource-library/" class="breadcrumbs__link">{{ __('Resources',
                        'visceral') }} {!! $resource_types !!}</a>
                </div>
                <h1 class="entry-title reveal">{!! get_the_title() !!}</h1>
            </header>
            <main class="entry-content single-resource-content__main row {{ $no_js_cookie }}">
                <div class="modal-bg">
                    <div class="modal download-modal">
                        <i class="icon icon-close" aria-label="Close">Close</i>
                        <h3>{{ __('Download Now', 'visceral' )}}</h3>
                        <?php 
                        echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');  
                        ?>
                    </div>
                </div>
                @if ($thumbnail_image)
                <div class="column md-25">
                    <div class="single-resource-content__main-image text-center reveal"><img
                            src="{{ $thumbnail_image['url'] }}" alt="{{ $thumbnail_image['alt'] }}"
                            width="{{ $thumbnail_image['width'] }}" height="{{ $thumbnail_image['height'] }}"></div>
                </div>
                @endif
                <div class="column {{ $thumbnail_image ? 'md-75' : 'md-100'}}">
                    <div class="single-resource-content__links-container reveal">
                        @if($resource_links)
                        <ul>
                            @foreach($resource_links as $resource_link)
                            <li><a href="{{ $resource_link->link }}"
                                    class="resource-link resource-link--{{$resource_link->type}} {{ $resource_link->gated ? 'resource-link--gated' : '' }}"
                                    target="_blank" rel="noopener noreferrer">{{ $resource_link->text }}</a> </li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                    @php(the_content())
                </div>
            </main>
            <div class="entry-meta reveal">
                @if($published_date)
                <p><span>Published</span> {{ $published_date }}</p>
                @endif
            </div>
        </div>
    </div>
</article>

@includeWhen($related_content, 'partials.related-content')

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author" => array(
				"@type" => "Person",
				"name" => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified" =>  get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>
<script type='application/ld+json'>
    <?php echo json_encode( $json_ld ); ?>
</script>
<?php endif; ?>