<article @php(post_class('row single-experience-content'))>
	<header class="column xs-100 md-67">
		<div class="single__header single-experience-content__header">         
			@if (has_post_thumbnail())
				<div class="img-circle swirl-img reveal reveal--delay">{{ the_post_thumbnail() }}</div>
			@endif
			<div class="breadcrumbs reveal">
				<a href="/our-work/our-experience/" class="breadcrumbs__link">Our Experience</a>
			</div>    
			<h1 class="entry-title reveal">{{ get_the_title() }}</h1>
		</div>
	</header>
	<main class="column xs-100">
		<div class="single-experience-content__main reveal">
			{{ the_content() }}
		</div>
	</main>
</article>  

@includeWhen($related_content, 'partials.related-content')

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author" => array(
				"@type" => "Person",
				"name" => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified" =>  get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>
		<script type='application/ld+json'>
			<?php echo json_encode( $json_ld ); ?>
		</script>
	<?php endif; ?>
