@php


// function getAuthorInfo($id) {
//     $output = '';
//     if (function_exists('get_field') && !empty(get_field('authors', $id))) {
//         $output = '<div class="authors__images">';

//         $authors = get_field('authors', $id);
//         foreach ($authors as $author) {
//             $published = $author->post_status === 'publish' ? true : false;
            
//             if ($published) {
//                 if (has_post_thumbnail($author->ID)) { 
//                     $output .= '<div class="img-circle">';
//                     $image = wp_get_attachment_image_src(get_post_thumbnail_id($author->ID), 'medium');
//                         $w     = $image[1];
//                         $h     = $image[2];
//                         $author_img_class = ( $w < $h ) ? 'portrait' : '';
                    
//                     $output .= get_the_post_thumbnail($author->ID, 'thumbnail', array('class' => $author_img_class)); 
//                     $output .= '</div>';
//                 }
//             }
//         }
//         $output .= '</div>';
//     }

//     return $output;
// }

$post_type        = get_post_type();
$post_id          = get_the_id();
$meta_output      = '';
$tax_term         = '';
$card_image_class = '';
$excerpt          = wp_trim_words(get_the_excerpt(), 20 );
$sector = ( 'event' == $post_type ) ? 'event_sectors' : $post_type . '_sector';
$sector = wp_get_post_terms( $post_id, $sector );

if ($post_type === 'post') {
    $meta_output = '<span class="meta-post-type">' . __('Insights & Perspectives', 'visceral') . '</span>';
    $tax_term = 'category';
} elseif ($post_type === 'resource') {
    // If we're on the resource library page we want to do something different
    if ($page_slug === 'resource-library') {
        $meta_output = '<span class="meta-post-type">';
    } else {
        $meta_output = '<span class="meta-post-type">' . __('Resources', 'visceral') . '</span>';
    }
    $tax_term = 'resource_type';
} elseif ($post_type === 'event') {
    $meta_output = '<span class="meta-post-type">' . __('Events', 'visceral') . '</span>';
    $tax_term = 'event_type';
} elseif ($post_type === 'impact_story') {
    $meta_output = '<span class="meta-post-type">' . __('Impact Stories', 'visceral') . '</span>';
}

$terms = wp_get_post_terms($post_id, $tax_term, array('fields' => 'names'));
$terms_output = '';
if (!empty($terms)) {
    foreach ($terms as $term) {
        $terms_output .= $term . ', ';
    }
    $terms_output = substr($terms_output, 0, -2);
}
$meta_output .= $terms_output;

// If we're on the resource library page we want to do something different
if ($post_type === 'resource' && $page_slug === 'resource-library') {
    $meta_output .= '</span>';
}
$featured_image = App\get_aspect_ratio_image(2, 1, '600x400');

if ($featured_image) {
    $card_image_class = 'list-item-card--featured-image';
}
@endphp

<article class="list-item-card list-item-card--{{$post_type}} {{$card_image_class}} column xs-100 md-50 lg-33 reveal">
    <div>
        <a href="{{ get_permalink() }}" class="list-item-card__link">
            @if(has_post_thumbnail())
                <div class="image-zoom">
                    <div class="list-item-card__image img-cover">
                        {!! $featured_image !!}
                    </div>
                </div>
            @endif
            <h3 class="list-item-card__title">{!! get_the_title() !!}</h3>
            <p class="list-item-card__excerpt small">{!! $excerpt !!}</p>
        </a>
        @if($meta_output)
            <div class="list-item-card__meta">
                {!! $meta_output !!}
            </div>
        @endif
    </div>    
</article>