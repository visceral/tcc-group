<?php
$column         = has_post_thumbnail() ? 'sm-67' : 'sm-100';
$featured_image = App\get_aspect_ratio_image(4, 3, 'medium');
$excerpt        = wp_trim_words(get_the_excerpt(), 50 );
$type           = get_post_type();
if( $type == 'impact_story') {
  $type = '<a href="/our-work/impact-stories/" class="btn btn--small btn--blue">Impact Stories</a>';
} elseif( $type == 'event') {
  $type = '<a href="/events/" class="btn btn--small btn--purple">Events</a>';
} elseif( $type == 'resource') {
  $type = '<a href="/insights-resources/resource-library/" class="btn btn--small btn--magenta">Resources</a>';
} elseif( $type == 'person') {
  $type = '<a href="/about-us/our-team/" class="btn btn--small btn--dark-orange">Our Team</a>';
} elseif( $type == 'service') {
  $type = '<a href="/our-work/our-services/" class="btn btn--small btn--light-green"> Our Services</a>';
} elseif( $type == 'experience') {
  $type = '<a href="/our-work/our-experience/" class="btn btn--small btn--light-green"> Our Experience</a>';
} elseif( $type == 'page') {
  $type = '<a href="/" class="btn btn--small btn--gray">Page</a>';
} else {
  $type = '<a href="/insights-resources/insights-perspectives/" class="btn btn--small btn--orange">Insights & Perspectives</a>';
}
?>

<article @php(post_class( 'row list-item-result' ))>
  <div class="column {!! $column !!}">
    <header>
      {!! $type !!}
      <h3 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
    </header>
    <div class="entry-summary">
      <p>{!! $excerpt !!}</p>
    </div>
  </div>
  <?php
  if (has_post_thumbnail()) {
      echo '<div class="column sm-33 text-center">' . $featured_image . '</div>';
  }
  ?>
</article>
