@extends('layouts.app')
@section('content')
@while(have_posts()) @php(the_post())
@include('partials.content-page')
@php
$form_filters = App::formFilters();
@endphp
@if($get_sectors)
<ul class="list-inline clients-filter reveal">
    <li class="clients-filter__item {{!$form_filters->sector ? 'clients-filter__item--active' : ''}}"><a
            href="{{ get_permalink()}}">{{ _e('All Clients', 'visceral') }}</a></li>
    @foreach($get_sectors as $sector)
    {{-- @php(var_dump($sector)) --}}
    <li
        class="clients-filter__item clients-filter__item--{{$sector->slug}} {{$form_filters->sector === $sector->slug ? 'clients-filter__item--active' : ''}}">
        <a href="{{ get_permalink()}}?sector={{$sector->slug}}">{{$sector->name}}</a></li>
    @endforeach
</ul>
@endif
@if($get_clients)
<div class="client-list">
    <div class="row">
        @foreach($get_clients->posts as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))

        @include('partials.list-item-client')
        @php(wp_reset_postdata())
        @endforeach
    </div>
</div>
@endif
@endwhile
@endsection