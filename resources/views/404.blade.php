@extends('layouts.app')

@section('content')
  {{-- @if (!have_posts()) --}}
  <div class="row justify-center">
    <div class="column md-67">
        <div class="alert alert-warning">
        {{ __('Sorry, but the page you were trying to view does not exist.', 'visceral') }}
        </div>
        <form action="<?php echo site_url(); ?>" class="searchform">
            <div class="row">
                <div class="column sm-67">
                    <label><span class="screen-reader-text">{{ __('Search', 'visceral')}}</span>
                    <input class="input--alt" type="text" name="s" value="" class="">
                    </label>
                </div>
                <div class="column sm-33">
                    <input type="submit" value="{{ __('Submit', 'visceral')}}">
                </div>
            </div>
          </form>
    </div>
  </div>      
  {{-- @endif --}}
@endsection
