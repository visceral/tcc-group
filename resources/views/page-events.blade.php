@extends('layouts.app')
@section('content')
@while(have_posts()) @php(the_post())
<div class="row justify-center">
    <div class="column xs-100 lg-80">
        @include('partials.content-page')
        <section class="event-filters">
            <ul class="list-inline">
                <li class="{{$past_events ? '' : 'event-filter--active'}}"><a href="/events">{{ __('Upcoming',
                        'visceral') }}</a></li>
                <li class="{{$past_events ? 'event-filter--active' : ''}}"><a href="/events?past-events=true">{{
                        __('Past', 'visceral') }}</a></li>
            </ul>
        </section>
        @if($get_events->posts)
        <section class="events-list">
            @foreach($get_events->posts as $post)
            @php(setup_postdata($GLOBALS['post'] = $post))

            @include('partials.list-item-'.get_post_type())
            @php(wp_reset_postdata())
            @endforeach
        </section>
        @includeWhen($get_events->max_num_pages > 1, 'partials.load-more')
        @else
        <p class="text-center">{{ __('Sorry, there are no upcoming events at this time.', 'visceral') }}</p>
        <p class="text-center">{{ __('Please check back soon and/or ', 'visceral') }}<a
                href="/events/?past-events=true">{{ __('view past events.', 'visceral') }}</a></p>

        @endif
    </div>
</div>
@endwhile
@endsection