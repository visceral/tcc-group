<?php

namespace App;


/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
});

add_filter('sage/template/list-item-person/data', function (array $data) {
    $data['title'] = get_the_title();
    return $data;
});

/**
 * Allow SVG Uploads
 */
function theme_allow_svg_uploads($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__.'\\theme_allow_svg_uploads');
// End Allow SVG Uploads

/**
 * Custom Font Formats
 */
function theme_tiny_mce_formats_button($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', __NAMESPACE__ . '\\theme_tiny_mce_formats_button');

// Callback function to filter the MCE settings
function theme_tiny_mce_custom_font_formats($init_array) {
    // Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title'    => 'Split Divider',
			'selector' => '*',
			'classes'  => 'split-divider'
        ),
        array(
			'title'    => 'Buffer',
			'selector' => '*',
			'classes'  => 'buffer'
		),
		array(
			'title'    => 'Button',
			'selector' => 'a',
			'classes'  => 'btn button'
        ),
        array(
			'title'    => 'Link with arrow',
			'selector' => 'a',
			'classes'  => 'link--icon'
		),
		array(
			'title'    => 'Number Ticker',
			'inline'   => 'span',
			'classes'  => 'ticker'
        ),
        array(
			'title'    => 'Intro',
			'selector' => 'p',
			'classes'  => 'intro'
		),
        array(
			'title'    => 'Cite',
			'selector' => 'p',
			'classes'  => 'cite'
        ),
        array(
            'title'   => 'Table',
            'items' => array(
                array(
                    'title' => 'Vertical Line Table',
                    'selector' => 'table',
                    'classes' => 'table--vertical-lines'
                ),
                array(
                    'title' => 'Grid Table',
                    'selector' => 'table',
                    'classes' => 'table--grid'
                ),
            ),
        ),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode($style_formats );
	return $init_array;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\theme_tiny_mce_custom_font_formats'); // Attach callback to 'tiny_mce_before_init'
// End Custom Font Formats

/**
 * Query for all post statuses so attachments are returned
 */
function visceral_modify_link_query_args($query ) {
	// Attachment post types have status of inherit. This allows them to be included.
	$query['post_status'] = array('publish', 'inherit');
	return $query;
}
add_filter('wp_link_query_args', __NAMESPACE__ . '\\visceral_modify_link_query_args' );

/**
 * Link to media file URL instead of attachment page
 */
function visceral_modify_link_query_results($results, $query ) {
  foreach ($results as &$result ) {
	if ('Media' === $result['info'] ) {
	  $result['permalink'] = wp_get_attachment_url($result['ID'] );
	}
  }
  return $results;
}

add_filter('wp_link_query', __NAMESPACE__ . '\\visceral_modify_link_query_results' );

/**
 * Filter Search by Post Type
 */
add_filter( 'searchwp\swp_query\args', function( $args ) {
  if ( isset( $_GET['filter'] ) ) {
    $args['post_type'] = array(sanitize_text_field($_GET['filter']));
  }
  return $args;
} );

function visceral_alm_query_args_searchwp($args) {
    $engine = 'resource_search'; // for resource search
    $args = apply_filters('alm_searchwp', $args, $engine); // Make call to alm_searchwp filter
    return $args;
}
add_filter('alm_query_args_searchwp', __NAMESPACE__ . '\\visceral_alm_query_args_searchwp');

/**
 * LOWER YOAST META BOX
 */
function yoast_dont_boast($html ) {
    return 'low';
}
add_filter('wpseo_metabox_prio', __NAMESPACE__ . '\\yoast_dont_boast' );

// END LOWER YOAST META BOX

function body_class($classes) {
    // Add class if no masthead is showing
    if (!visceral_has_masthead()) {
        $classes[] = 'no-masthead';
    }

    return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

// Gravity Forms
// Only show the survey question if the survey checkbox is checked on the resource page

function show_survey_question($form) {
  foreach ( $form['fields'] as &$field ) {
    if ( $field->id == 10 ) {
      global $post;
      if (function_exists('get_field') && !empty(get_field('links', $post->ID))) {
        foreach (get_field('links', $post->ID) as $resource_link) {
          if (!empty($resource_link['survey']) && $resource_link['survey'] === true) {
            $field->visibility = 'visibile';
          }
        }
      }
    }
  }
  return $form;
}
add_filter( 'gform_pre_render_2', __NAMESPACE__ . '\\show_survey_question' );

add_filter( 'gform_submit_button_3', __NAMESPACE__ . '\\input_to_button', 10, 2 );
function input_to_button( $button, $form ) {
     
    $dom = new \DOMDocument();
    $dom->loadHTML( '<?xml encoding="utf-8" ?>' . $button );
    $input = $dom->getElementsByTagName( 'input' )->item(0);
    $new_button = $dom->createElement( 'button' );
    $new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
    $input->removeAttribute( 'value' );
   
    foreach( $input->attributes as $attribute ) {
        $new_button->setAttribute( $attribute->name, $attribute->value );
    }
     $classes = $input->getAttribute( 'class' );
    $classes .= " btn--purple btn--mail";
    $new_button->setAttribute( 'class', $classes );
    $input->parentNode->replaceChild( $new_button, $input );
 
    return $dom->saveHtml( $new_button );
}