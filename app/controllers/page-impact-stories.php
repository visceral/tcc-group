<?php
namespace App;

use Sober\Controller\Controller;




class ImpactStories extends Controller
{
    public function getImpactStories()
    {
        $story_sector = !empty($_REQUEST['story_sector']) ? sanitize_text_field($_REQUEST['story_sector']) : '';

        $tax_query = array(
            'relation'   => 'AND'
        );

        // If there is a type filter, apply it
        if (!empty($story_sector)) {
            $tax_query[] = array(
                        'taxonomy' => 'story_sector',
                        'field'    => 'slug',
                        'terms'    => $story_sector,
                    );
        }

        $args = array(
        'post_type'              => 'impact_story',
        'posts_per_page'         => 4,
        'tax_query'              => $tax_query,
        'update_post_term_cache' => false, // Improves Query performance
        'update_post_meta_cache' => false, // Improves Query performance
        );

        $query = new \WP_Query($args);

        return $query;
    }

    public function featuredImpactStory()
    {
        if (function_exists('get_field')) {
            $featured_impact_stories = get_field('featured_impact_story');

            if (!empty($featured_impact_stories)) {
                return $featured_impact_stories[0];
            }
        } else {
            return false;
        }
    }

    public function featuredImpactStoryFeaturedImage()
    {
        $featured_image = get_aspect_ratio_image(2, 1, 'full');
        return $featured_image;
    }


    public function getImpactStorySectors()
    {
        return get_terms('story_sector');
    }

    public function storySectorFilter()
    {
        $story_sector_filter = !empty($_REQUEST['story-sector']) ? sanitize_text_field($_REQUEST['story-sector']) : '';
        return $story_sector_filter;
    }

    public function ajaxLoadMoreArgs()
    {
        $form_filters = App::formFilters();
        // AJAX load more posts plugin (if active) - https://wordpress.org/plugins/ajax-load-more/
        if ( shortcode_exists( 'ajax_load_more' ) ) :
            // Build up args
            $ajax_load_args = '
            container_type="div" 
            theme_repeater="list-item-impact_story.php"
            post_type="impact_story"
            offset="4"
            posts_per_page="4"
            pause="true"
            scroll="false"
            button_label="More"
            button_loading_label="Loading..."
            container_type="div"
            css_classes=""';

            if ( $form_filters->story_sector ) {
                $ajax_load_args .= ' taxonomy="story_sector" taxonomy_terms="' . $form_filters->story_sector . '" taxonomy_operator="IN"';
            }

            // Run the shortcode
            return $ajax_load_args;
        else :
            return false;
        endif;
        // Ajax plugin
    }
}
