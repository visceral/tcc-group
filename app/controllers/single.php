<?php 


namespace App;

use Sober\Controller\Controller;

class Single extends Controller
{
    public function getMainCategory() {
        $terms = wp_get_post_terms(get_the_ID(), 'category');
        $output = '';
    
        if (!empty($terms)) {
            $output = '<span>' . $terms[0]->name . '</span>';
        }
        return '<span class="tax-divider"> • </span>' . $output;
    }

    public function postSector()
    {
        $post_sectors = wp_get_post_terms(get_the_id(), 'post_sector', array('fields' => 'names'));

        return $post_sectors[0];
    }

    public function authors()
    {
        $authors_output = [];
        if ((function_exists('get_field') && !empty(get_field('authors'))) || !empty(get_field('custom_authors'))) {
            $author_names_array = array();
            $author_images = '<div class="authors__images">';
            // Staff Authors
            if (function_exists('get_field') && !empty(get_field('authors'))) {
                $authors = get_field('authors');
        
                foreach ($authors as $author) {
                    $author_output = new \stdClass();
                    $ID = $author->ID;
                    $author_output->ID = $ID;
                    $published = $author->post_status === 'publish' ? true : false;
                    $author_output->published = $published;
                    
                    // if ($published) {
                        $author_output->name = $author->post_title;
                        if (get_field('role', $author->ID)) $author_output->role = get_field('role', $author->ID);
                        if (has_post_thumbnail($ID)) {
                            $author_output->image = '<div class="img-circle">';
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($ID), 'medium');
                                $w     = $image[1];
                                $h     = $image[2];
                                $author_img_class = ( $w < $h ) ? 'portrait' : '';
                            
                            $author_output->image .= get_the_post_thumbnail($ID, 'thumbnail', array('class' => $author_img_class)); 
                            $author_output->image .= '</div>';
                        }
                    // }

                    $authors_output[] = $author_output;
                }
            }
        
            // Custom Authors
            if (function_exists('have_rows') && !empty(have_rows('custom_authors'))) {
                while ( have_rows('custom_authors') ) : the_row();
                    // Check if name provided (don't show image if no name)
                    if (get_sub_field('custom_author_name') !== "") {
                        $author_output = new \stdClass();
                        $custom_author_name = get_sub_field('author_name');
                        $author_output->name = $custom_author_name;
                        $custom_author_role = get_sub_field('author_title');
                        $author_output->role = $custom_author_role;
                        // Check if image provided
                        if (!empty(get_sub_field('author_image'))) {
                            $custom_author_image = get_sub_field('author_image');
                            $custom_author_image = $custom_author_image['sizes'];
                            $custom_author_image['thumbnail'];
                            $author_img_class = ( $custom_author_image['thumbnail-width'] < $custom_author_image['thumbnail-height'] ) ? 'portrait' : '';
        
                            $author_output->image = '<div class="img-circle">';
                            $author_output->image .= '<img src="' . $custom_author_image['thumbnail'] . '" alt="' . $custom_author_name . '" class="' . $author_img_class . '">';
                            $author_output->image .= '</div>'; 
                        }

                        $authors_output[] = $author_output;
                    }
                endwhile;
            }
        }

        return $authors_output;
    }

    public function relatedContent()
    {
        $related_content = visceral_related_posts(3, array('post', 'event', 'resource', 'impact_story'), Single::postSector(), 'related_content');
        return $related_content;
    }
}
