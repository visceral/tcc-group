<?php
namespace App;

use Sober\Controller\Controller;

class InsightsPerspectives extends Controller
{
    public function getPosts()
    {
        $type_filter = !empty($_REQUEST['type']) ? sanitize_text_field($_REQUEST['type']) : '';
        $sector_filter = !empty($_REQUEST['sector']) ? sanitize_text_field($_REQUEST['sector']) : '';

        $tax_query = array('relation' => 'AND');

        if ($type_filter) {
            $tax_query[] = array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => $type_filter,
            );
        }

        if ($sector_filter) {
            $tax_query[] = array(
                'taxonomy' => 'post_sector',
                'field'    => 'slug',
                'terms'    => $sector_filter,
            );
        }

        $args = array(
        'post_type'              => 'post',
        'post_status'            => 'publish',
        'posts_per_page'         => 5,
        'orderby'                => 'date',
        'order'                  => 'DESC',
        'tax_query'              => $tax_query,
        'update_post_term_cache' => false, // Improves Query performance
        'update_post_meta_cache' => false, // Improves Query performance
        );
        $query = new \WP_Query($args);
        return $query;
    }

    public function getPostTypes()
    {
        return get_terms('category');
    }

    public function getPostSectors()
    {
        return get_terms('post_sector');
    }

    public function pagination()
    {
        $insights     = InsightsPerspectives::getPosts();
        $total        = $insights->max_num_pages;
        $big          = 999999999; // need an unlikely integer
        $current_page = max( 1, get_query_var('paged') );
        $args         = array(
            'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'    => '?paged                                = %#%',
            'current'   => $current_page,
            'total'     => $total,
            'prev_text' => '<span class="icon icon-chevron-right"></span>' . __(' Previous', 'visceral'),
            'next_text' => __('Next ', 'visceral') . '<span class="icon icon-chevron-right"></span>'
        );
    
        return paginate_links( $args );
    }

    public function ajaxLoadMoreArgs()
    {
        $form_filters = App::formFilters();
        // AJAX load more posts plugin (if active) - https://wordpress.org/plugins/ajax-load-more/
        if ( shortcode_exists( 'ajax_load_more' ) ) :
            // Build up args
            $ajax_load_args = '
            container_type="div" 
            theme_repeater="list-item-post.php"
            post_type="post"
            offset="5"
            posts_per_page="5"
            orderby="date"
            pause="true"
            scroll="false"
            button_label="More"
            button_loading_label="Loading..."
            container_type="div"';
            if ( $form_filters->type ) {
                $ajax_load_args .= ' taxonomy="category" taxonomy_terms="' . $form_filters->type . '" taxonomy_operator="IN"';
            }

            if ( $form_filters->sector ) {
                $ajax_load_args .= ' taxonomy="post_sector" taxonomy_terms="' . $form_filters->sector . '" taxonomy_operator="IN"';
            }
            // Run the shortcode
            return $ajax_load_args;
        else :
            return false;
        endif;
        // Ajax plugin
    }
}