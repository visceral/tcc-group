<?php
namespace App;

use Sober\Controller\Controller;




class ResourceLibrary extends Controller
{
    public function getResources()
    {
        $keyword = !empty($_REQUEST['resource_keyword']) ? sanitize_text_field($_REQUEST['resource_keyword']) : '';
        $resource_type = !empty($_REQUEST['resource_type']) ? sanitize_text_field($_REQUEST['resource_type']) : '';
        $resource_sector = !empty($_REQUEST['resource_sector']) ? sanitize_text_field($_REQUEST['resource_sector']) : '';

        $tax_query = array(
            'relation'	   => 'AND'
        );

        // If there is a type filter, apply it
        if ( !empty($resource_type) ) {
        $tax_query[] = array(
                        'taxonomy' => 'resource_type',
                        'field'    => 'slug',
                        'terms'    => $resource_type,
                    );
        }

        // If there is a type filter, apply it
        if ( !empty($resource_sector) ) {
        $tax_query[] = array(
                        'taxonomy' => 'resource_sector',
                        'field'    => 'slug',
                        'terms'    => $resource_sector,
                    );
        }

        $args = array(
        'post_type'              => 'resource',
        'posts_per_page'         => 9,
        'orderby'                => 'date',
        'order'                  => 'DESC',
        'tax_query'              => $tax_query,
        'update_post_term_cache' => false, // Improves Query performance
        'update_post_meta_cache' => false, // Improves Query performance
        );

        $featured_resource = ResourceLibrary::featuredResource();
        if ($featured_resource) {
            $args['post__not_in'] = array($featured_resource->ID);
        }

        // If a search keyword was entered and we have SearchWP installed, add the keyword to the query and use SearchWP
        if ($keyword !== '' && class_exists( 'SWP_Query' )) {
            $args['s'] = $keyword;
            $args['engine'] = 'resource_search';

            $query = new \SWP_Query($args);
            
        // If no keyword, use regular query 	
        } else {
            $query = new \WP_Query($args); 
        }
        $query = new \WP_Query($args); 
        return $query;
    }

    public function featuredResource()
    {
        // Get filters
        $form_filters = App::formFilters();
        // Covert filter object to assoc. array with obj properties
        $filters_array = (array) $form_filters;
        // If the filter array's values are empty or false, get the featured resource
        if (function_exists('get_field') && !array_filter($filters_array)) {
            $featured_impact_stories = get_field('featured_resource');

            if (!empty($featured_impact_stories)) {
                return $featured_impact_stories[0];
            }
        } else {
            return false;
        }
    }

    public function featuredResourceTypes()
    {
        $featured_resource = ResourceLibrary::featuredResource();
        $featured_resource_resource_types_output = '';
        $featured_resource_resource_types = wp_get_post_terms($featured_resource->ID, 'resource_type', array( 'fields' => 'names' ));

        foreach ($featured_resource_resource_types as $resource_type) {
            $featured_resource_resource_types_output .= $resource_type . ', ';
        }
        $featured_resource_resource_types_output = substr($featured_resource_resource_types_output, 0, -2);
        return $featured_resource_resource_types_output;
    }

    public function getResourceTypes()
    {
        return get_terms('resource_type');
    }

    public function getResourceSectors()
    {
        return get_terms('resource_sector');
    }

    public function keywordValue()
    {
        $keyword_value = !empty($_REQUEST['resource-keyword']) ? sanitize_text_field($_REQUEST['resource-keyword']) : '';
        return $keyword_value;
    }

    public function pagination()
    {
        $insights     = ResourceLibrary::getResources();
        $total        = $insights->max_num_pages;
        $big          = 999999999; // need an unlikely integer
        $current_page = max( 1, get_query_var('paged') );
        $args         = array(
            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'  => '?paged=%#%',
            'current' => $current_page,
            'total'   => $total,
            'prev_text' => '<span class="icon icon-chevron-right"></span>' . __(' Previous', 'visceral'),
            'next_text' => __('Next ', 'visceral') . '<span class="icon icon-chevron-right"></span>'
        );
    
        return paginate_links( $args );
    }

    public function ajaxLoadMoreArgs()
    {
        $form_filters = App::formFilters();
        // AJAX load more posts plugin (if active) - https://wordpress.org/plugins/ajax-load-more/
        if ( shortcode_exists( 'ajax_load_more' ) ) :
            // Build up args
            $ajax_load_args = '
            container_type="div" 
            theme_repeater="list-item-resource.php"
            post_type="resource"
            offset="9"
            posts_per_page="9"
            pause="true"
            scroll="false"
            button_label="More"
            button_loading_label="Loading..."
            container_type="div"
            css_classes="row"';
            if ( $form_filters->resource_type ) {
                $ajax_load_args .= ' taxonomy="resource_type" taxonomy_terms="' . $form_filters->resource_type . '" taxonomy_operator="IN"';
            }

            if ( $form_filters->resource_sector ) {
                $ajax_load_args .= ' taxonomy="resource_sector" taxonomy_terms="' . $form_filters->resource_sector . '" taxonomy_operator="IN"';
            }

            if ( $form_filters->resource_keyword ) {
                $ajax_load_args .= 'id="searchwp" search="'. $form_filters->resource_keyword .'"';
            }
            
            // Run the shortcode
            return $ajax_load_args;
        else :
            return false;
        endif;
        // Ajax plugin
    }
}
