<?php

namespace App;

use Sober\Controller\Controller;

class Page extends Controller
{
    public function subHeading()
    {
        if (!empty(get_post_meta(get_the_id(), 'sub-heading', true))) {
            return get_post_meta(get_the_id(), 'sub-heading', true);
        } else {
            return '';
        }
    }

}
