<?php

namespace App;

use Sober\Controller\Controller;

class SingleResource extends Controller
{
    public function noJsCookie()
    {
        $no_js_cookie = 'cookie';
        if( isset($_COOKIE['tcc_resource_download_gate']) ) {
            $no_js_cookie = 'no-resource-gate';
        }
        return $no_js_cookie;
    }

    public function resourceTypes()
    {
        $resource_types = wp_get_post_terms(get_the_id(), 'resource_type', array('fields' => 'names'));
        $resource_types_output = '';

        if (!empty($resource_types)) {
            foreach ($resource_types as $resource_type) {
                $resource_types_output .= $resource_type . ', ';
            }

            $resource_types_output = substr($resource_types_output, 0, -2);
        }
        return '<span class="tax-divider"> • </span>' . '<span>' . $resource_types_output . '</span>';
    }

    public function resourceSector()
    {
        $resource_sectors = wp_get_post_terms(get_the_id(), 'resource_sector', array('fields' => 'names'));

        return $resource_sectors[0];
    }

    public function resourceLinks()
    {
        $resource_links_output = [];

        if (function_exists('get_field') && !empty(get_field('links'))) {
            foreach (get_field('links') as $resource_link) :
                $link_obj = new \stdClass();
                $link_obj->gated = false;
                if ($resource_link['download_type'] === 'File') {
                    $link_obj->link = $resource_link['file']['url'];
                    $link_obj->type = 'file';
                } elseif ($resource_link['download_type'] === 'Link') {
                    $link_obj->link = $resource_link['link'];
                    $link_obj->type = 'url';
                }
                if (!empty($resource_link['gated']) && $resource_link['gated'] === true) {
                    $link_obj->gated = true;
                }
                $link_obj->text = $resource_link['link_text'] ? $resource_link['link_text'] : __('Download', 'visceral');
                $resource_links_output[] = $link_obj;
            endforeach;
        }

        return $resource_links_output;
    }

    public function publishedDate()
    {
        $date = '';
        if (function_exists('get_field')) {
            $date = date_format(date_create(get_field('published_date')), 'F Y');
        }
        return $date;
    }

    public function thumbnailImage()
    {
        $thumbnail_image = '';
        if (function_exists('get_field')) {
            $thumbnail_image = get_field('thumbnail_image');
        }
        
        
        return $thumbnail_image;
    }

    public function relatedContent()
    {
        $related_content = visceral_related_posts(3, array('post', 'event', 'resource', 'impact_story'), SingleResource::resourceSector(), 'related_content');
        return $related_content;
    }
}
