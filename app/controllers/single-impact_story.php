<?php

namespace App;

use Sober\Controller\Controller;

class SingleImpactStory extends Controller
{
    public function servicesAndExperiences()
    {
        $output = '';
        $services = '';
        $experiences = '';
        if (function_exists('get_field')) {
            $services = get_field('services');
            if (!empty($services)) {
                foreach ($services as $service) {
                    $output .= '<a href="' . get_permalink($service->ID) . '">' . $service->post_title . '</a>, ';
                }
            }

            $experiences = get_field('experiences');
            if (!empty($experiences)) {
                foreach ($experiences as $experience) {
                    $output .= '<a href="' . get_permalink($experience->ID) . '">' . $experience->post_title . '</a>, ';
                }  
            }

            $output = substr($output, 0, -2);

        }
        return $output;
    }

    public function storySector()
    {
        $story_sectors = wp_get_post_terms(get_the_id(), 'story_sector', array('fields' => 'names'));

        return $story_sectors[0];
    }

    public function relatedContent()
    {
        $related_content = visceral_related_posts(3, array('post', 'event', 'resource', 'impact_story'), SingleImpactStory::storySector(), 'related_content');
        return $related_content;
    }
}
