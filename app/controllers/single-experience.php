<?php

namespace App;

use Sober\Controller\Controller;

class SingleExperience extends Controller
{
    public function relatedContent()
    {
        $related_content = visceral_related_posts(3, array('post', 'resource', 'impact_story'), false, 'related_content');
        return $related_content;
    }
}
