<?php

namespace App;

use Sober\Controller\Controller;

class OurTeam extends Controller
{
    public function getLeadership()
    {
        $args = array(
            'post_type'              => 'person',
            'posts_per_page'         => -1,
            'post_status'            => 'publish',
            'meta_key'               => 'last_name',
            'orderby'                => 'meta_value',
            'order'                  => 'ASC',
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
            'tax_query' => array(
                array(
                    'taxonomy' => 'department',
                    'field'    => 'slug',
                    'terms'    => 'leadership-team'
                )
            )
        );

        $people_query = new \WP_Query($args);
        return $people_query;
    }

    public function getDepartments()
    {
        return get_terms('department');
    }

    public function getStaff()
    {
        $args = array(
            'post_type'              => 'person',
            'posts_per_page'         => -1,
            'post_status'            => 'publish',
            'meta_key'               => 'last_name',
            'orderby'                => 'meta_value',
            'order'                  => 'ASC',
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
            'tax_query' => array(
                array(
                    'taxonomy' => 'department',
                    'field'    => 'slug',
                    'terms'    => 'consulting-staff'
                )
            )
        );

        $people_query = new \WP_Query($args);
        return $people_query;
    }

    public function getAffiliates()
    {
        $args = array(
            'post_type'              => 'person',
            'posts_per_page'         => -1,
            'post_status'            => 'publish',
            'meta_key'               => 'last_name',
            'orderby'                => 'meta_value',
            'order'                  => 'ASC',
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
            'tax_query' => array(
                array(
                    'taxonomy' => 'department',
                    'field'    => 'slug',
                    'terms'    => 'affiliate'
                )
            )
        );

        $people_query = new \WP_Query($args);
        return $people_query;
    }
}
