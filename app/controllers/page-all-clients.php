<?php
namespace App;

use Sober\Controller\Controller;




class AllClients extends Controller
{
    public function getClients()
    {
        $sector = !empty($_REQUEST['sector']) ? sanitize_text_field($_REQUEST['sector']) : '';

        $tax_query = array(
            'relation'	   => 'AND'
        );

        // If there is a type filter, apply it
        if (!empty($sector)) {
        $tax_query[] = array(
                        'taxonomy' => 'sector',
                        'field'    => 'slug',
                        'terms'    => $sector,
                    );
        }

        $args = array(
        'post_type'              => 'client',
        'post_status'            => 'publish',
        'orderby'                => 'title',
        'order'                  => 'ASC',
        'posts_per_page'         => -1,
        'tax_query'              => $tax_query,
        'update_post_term_cache' => false, // Improves Query performance
        'update_post_meta_cache' => false, // Improves Query performance
        );

        $query = new \WP_Query($args); 

        return $query;
    }

    public function getSectors()
    {
        return get_terms('sector', array('exclude' => array( 19 ), 'parent' => 0));
    }
}
