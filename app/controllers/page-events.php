<?php
namespace App;

use Sober\Controller\Controller;

class Events extends Controller
{
    public function getEvents()
    {
        $past_events = !empty($_REQUEST['past-events']) ? sanitize_text_field($_REQUEST['past-events']) : '';
        $today = current_time( "Ymd" );
        $args = array(
        'post_type'              => 'event',
        'posts_per_page'         => 6,
        'meta_query'             => array(
                                        array(
                                            'key' 		=> 'date',
                                            'value'		=> $today,
                                            'compare'	=> '>='
                                        )
                                    ),    
        'meta_key'   			=> 'date',
        'orderby'    			=> 'meta_value_num',
        'order'					=> 'ASC',
        // 'update_post_term_cache' => false, // Improves Query performance
        // 'update_post_meta_cache' => false, // Improves Query performance
        );

        if ($past_events) {
            $args['meta_query'] = array(
                        array(
                            'key' 		=> 'date',
                            'value'		=> $today,
                            'compare'	=> '<'
                        )
            );

            $args['order'] = 'DESC';

        }
        $query = new \WP_Query($args);
        return $query;
    }

    public function pastEvents() {
        $past_events = !empty($_REQUEST['past-events']) ? sanitize_text_field($_REQUEST['past-events']) : '';
        if ($past_events) {
            return true;
        } else {
            return false;
        }
    }

    public function ajaxLoadMoreArgs()
    {
        $past_events = !empty($_REQUEST['past-events']) ? sanitize_text_field($_REQUEST['past-events']) : '';
        $today = current_time( "Ymd" );

        // AJAX load more posts plugin (if active) - https://wordpress.org/plugins/ajax-load-more/
        if ( shortcode_exists( 'ajax_load_more' ) ) :
            // Build up args
            $ajax_load_args = '
            container_type="div" 
            theme_repeater="list-item-event.php"
            post_type="event"
            offset="6"
            posts_per_page="6"
            pause="true"
            meta_key="date"
            meta_value="' . $today . '"
            orderby="meta_value_num"
            order="DESC"
            scroll="false"
            button_label="More"
            button_loading_label="Loading..."
            container_type="div"';

            if ($past_events) {
                $ajax_load_args .= ' meta_compare="&lt;"';
            } else {
                $ajax_load_args .= ' meta_compare="&gt;"';
            }
            
            return $ajax_load_args;
        else :
            return false;
        endif;
        // Ajax plugin
    }
}
