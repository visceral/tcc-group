<?php
namespace App;

use Sober\Controller\Controller;

class Services extends Controller
{
    public function getServices()
    {
        $args = array(
        'post_type'                 => 'post',
        'posts_per_page'            => -1,
        // 'tax_query'              => array(
        //                                 array(
        //                                     'taxonomy' => 'person_type',
        //                                     'field'    => 'slug',
        //                                     'terms'    => 'staff'
        //                                 )
        //                          ),
        // 'meta_key'               => 'last_name',
        'orderby'                => 'menu_order',
        // 'order'                  => 'ASC',
        // 'update_post_term_cache' => false, // Improves Query performance
        // 'update_post_meta_cache' => false, // Improves Query performance
        );
        $query = new \WP_Query($args);
        return $query;
    }
}
