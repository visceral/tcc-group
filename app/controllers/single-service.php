<?php

namespace App;

use Sober\Controller\Controller;

class SingleService extends Controller
{
    public function relatedContent()
    {
        $related_content_posts = visceral_related_posts(3, array('post', 'resource', 'impact_story'), '', 'related_content');
        return $related_content_posts;
    }
}
