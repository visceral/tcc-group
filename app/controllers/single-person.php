<?php

namespace App;

use Sober\Controller\Controller;

class SinglePerson extends Controller
{
    public function role()
    {
        return get_post_meta(get_the_id(), 'role', true);
    }

    public function organization()
    {
        return get_post_meta(get_the_id(), 'organization', true);
    }
    
    public function firstName()
    {
        return get_post_meta(get_the_id(), 'first_name', true);
    }

    public function email()
    {
        return get_post_meta(get_the_id(), 'email_address', true);
    }

    public function insights()
    {
        $query = SinglePerson::get_related_content('post', 'authors');
        return $query;
    }

    public function events()
    {
        $query = SinglePerson::get_related_content('event', 'related_people');
        return $query;
    }

    public function resources()
    {
        $query = SinglePerson::get_related_content('resource', 'people');
        return $query;
    }

    static function get_related_content($post_type, $relationship_field) {
        $return = get_posts(array(
            'post_type' => $post_type,
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key' => $relationship_field,
                    'value' => '"' . get_the_id() . '"',
                    'compare' => 'LIKE'
                )
            )
        ));
        return $return;
    }
}
