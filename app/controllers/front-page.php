<?php

namespace App;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function firstHeadline()
    {
        return get_post_meta(get_the_id(), 'first_headline', true);
    }

    public function secondHeadline()
    {
        return get_post_meta(get_the_id(), 'second_headline', true);
    }

    public function homepageMastheadPromos()
    {
        if (function_exists('get_field')) {
            return get_field('promos');
        } else {
            return false;
        }
    }

    public function homepageVideo()
    {
        $video_url = get_post_meta(get_the_id(), 'background_video', true);
        $video = '';

        if (!empty($video_url)) {
            $video = '<div class="home-intro-slide home-intro-slide-video-bg">
                <iframe
                    src="' . $video_url  . '?autoplay=true&loop=true&muted=true&preload=auto&controls=false"
                    allow="accelerometer; gyroscope; autoplay; encrypted-media; picture-in-picture;"
                    allowfullscreen="true"
                    class="homepage-video"
                    id="home-masthead-video"
                ></iframe>
            </div>';
            // return '<div id="bgndVideo" class="player" data-property="{videoURL:\'\',containment:\'.homepage-masthead\', autoPlay:true, mute:true, stopMovieOnBlur: false, showYTLogo: false, showControls: false, loop: true, useOnMobile: false}"></div>';
        }

        return $video;
    }
    
    public function homepageVideoFallbackImage()
    {
        $mobile_image_url = get_the_post_thumbnail_url(get_the_id());

        if (!empty($mobile_image_url)) {
            return 'style="background-image: url(' . $mobile_image_url . ');"';
        } else {
            return false;
        }
    }
}
