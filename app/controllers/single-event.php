<?php

namespace App;

use Sober\Controller\Controller;

class SingleEvent extends Controller
{
    public function getMainEventType() {
        $terms = wp_get_post_terms(get_the_id(), 'event_type', array('fields' => 'names'));
        $output = '';
    
        if (!empty($terms)) {
            $output = '<span class="tax-divider"> • </span>';
            $output .= '<span>' . $terms[0] . '</span>';
        }
        return  $output;
    }

    public function eventDate()
    {
        $date = '';
        if (function_exists('get_field')) {
            $date = date_format(date_create(get_field('date')), 'F j, Y');

            if (!empty($date) && !empty(get_field('end_date'))) {
                $date .= ' - ';
            }

            if (!empty(get_field('end_date'))) {
                $date .= date_format(date_create(get_field('end_date')), 'F j, Y');
            }
        }
        return $date;
    }

    public function eventTime()
    {
        $time = '';
        if (function_exists('get_field')) {
            $time = get_field('start_time');

            if (!empty($time) && !empty(get_field('end_time'))) {
                $time .= ' - ';
            }

            if (!empty(get_field('end_time'))) {
                $time .= get_field('end_time');
            }
        }
        return $time;
    }

    public function eventLocation()
    {
        $location = '';

        if (function_exists('get_field')) {
            $location = get_field('location');
        }

        return $location;
    }

    public function eventButtonUrl()
    {
        if (!empty(get_post_meta(get_the_id(), 'event_website', true))) {
            return get_post_meta(get_the_id(), 'event_website', true);
        } else {
            return false;
        }
    }

    public function eventButtonText()
    {
        if (!empty(get_post_meta(get_the_id(), 'button_text', true))) {
            return get_post_meta(get_the_id(), 'button_text', true);
        } else {
            return false;
        }
    }
}
