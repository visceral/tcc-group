<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    // stream
    if (is_front_page()) {
        wp_enqueue_script('cloudflare-stream', 'https://embed.videodelivery.net/embed/sdk.latest.js');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_menu' => __('Primary Menu', 'visceral'),
        'mobile_menu' => __('Mobile Menu', 'visceral'),
        'footer_menu' => __('Footer Menu', 'visceral')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');
    add_image_size('600x400', 600, 400, true);

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Remove frontend CSS applied to admin bar
 */
function remove_admin_login_header()
{
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', __NAMESPACE__ . '\\remove_admin_login_header');

/**
 * WPBakery Page Builder overrides
 *
 * Hooks into 'init' action
 * 
 */
if (function_exists("vc_remove_element")) {
    // Remove default elements
    vc_remove_element('vc_facebook');
    vc_remove_element('vc_tweetmeme');
    vc_remove_element('vc_googleplus');
    vc_remove_element('vc_pinterest');
    vc_remove_element('vc_widget_sidebar');
    vc_remove_element('vc_flickr');
    vc_remove_element('vc_basic_grid ');
    vc_remove_element('vc_media_grid ');
    vc_remove_element('vc_wp_search');
    vc_remove_element('vc_wp_meta');
    vc_remove_element('vc_wp_recentcomments');
    vc_remove_element('vc_wp_calendar');
    vc_remove_element('vc_wp_pages');
    vc_remove_element('vc_wp_tagcloud');
    vc_remove_element('vc_wp_text');
    vc_remove_element('vc_wp_categories');
    vc_remove_element('vc_wp_archives');
    vc_remove_element('vc_wp_rss');

    // Remove extraneous options from buttons element
    vc_remove_param("vc_btn", "shape");
    vc_remove_param("vc_btn", "custom_background");
    vc_remove_param("vc_btn", "gradient_color_1");
    vc_remove_param("vc_btn", "gradient_color_2");
    vc_remove_param("vc_btn", "gradient_custom_color_1");
    vc_remove_param("vc_btn", "gradient_custom_color_2");
    vc_remove_param("vc_btn", "gradient_text_color");
    vc_remove_param("vc_btn", "gradient_color_2");
    vc_remove_param("vc_btn", "custom_text");
    vc_remove_param("vc_btn", "outline_custom_color");
    vc_remove_param("vc_btn", "outline_custom_hover_background");
    vc_remove_param("vc_btn", "outline_custom_hover_text");
}

function vc_update_defaults()
{
    if (function_exists('vc_update_shortcode_param')) {
        // Add custom colors to buttons
        $colors_arr = array(
            __('Blue (Primary)', 'js_composer') => 'blue',
            __('Purple', 'js_composer')         => 'purple',
            __('Magenta', 'js_composer')        => 'magenta',
            __('Orange', 'js_composer')         => 'orange',
            __('Dark Purple', 'js_composer')    => 'dark-purple',
        );
        $param = \WPBMap::getParam('vc_btn', 'color');
        $param['value'] = $colors_arr;
        vc_update_shortcode_param('vc_btn', $param);

        // Set style options
        $styles_arr = array(
            __('Pill (Primary)', 'js_composer') => 'pill',
            __('Outline', 'js_composer')        => 'outline',
        );
        $param = \WPBMap::getParam('vc_btn', 'style');
        $param['value'] = $styles_arr;
        vc_update_shortcode_param('vc_btn', $param);
    }
}

add_action('vc_after_init', __NAMESPACE__ . '\\vc_update_defaults');
