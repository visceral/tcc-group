<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    if (remove_action('wp_head', 'wp_enqueue_scripts', 1)) {
        wp_enqueue_scripts();
    }

    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                        "{$template}.blade.php",
                        "{$template}.php",
                    ];
                });
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

/* SHORTCODES */

/**
 * Visceral Query
 *
 * @return  The markup for a list of posts based on query parameters and template files.
 */
function visceral_query_shortcode($atts)
{
    extract(shortcode_atts(array(
        'container_class' => '',
        'template'        => 'views/partials/list-item',
        'show_pagination' => 0,
        'no_posts'        => '',
        'post_type'       => 'post',
        'posts_per_page'  => 6,
        'offset'          => 0,
        'order'           => 'DESC',
        'orderby'         => 'post_date',
        'post__not_in'    => '',
        'taxonomy'        => '',
        'terms'           => ''
    ), $atts));

    $markup = '';

    $args = array(
        'post_type'      => explode(', ', $post_type),
        'posts_per_page' => $posts_per_page,
        'offset'         => $offset,
        'order'          => $order,
        'orderby'        => $orderby,
        'post__not_in'   => explode(', ', $post__not_in),
    );
    if ($taxonomy && $terms) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => explode(', ', $terms),
            ),
        );
    }
    $query = new \WP_Query($args);

    if ($query->have_posts()) :
        $markup .= '<div class="' . $container_class . '">';
        while ($query->have_posts()) : $query->the_post();
            ob_start();
            include \App\template_path(locate_template($template . '-' . $post_type . '.blade.php'));
            $markup .= ob_get_contents();
            ob_end_clean();
        endwhile;
        $markup .= '</div>';
        wp_reset_postdata();

        // Pagination
        $total = $query->max_num_pages;
        // only bother with pagination if we have more than 1 page
        if ($show_pagination && $total > 1) :
            $big = 999999999; // need an unlikely integer
            $current_page = max(1, get_query_var('paged'));
            $pagination = paginate_links(array(
                'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format'    => '?paged=%#%',
                'current'   => $current_page,
                'total'     => $total,
                'type'      => 'plain',
                'prev_next' => true,
                'prev_text' => '<span class="icon-angle-left">' . __('Previous', 'visceral') . '</span>',
                'next_text' => '<span class="icon-angle-right">' . __('Next', 'visceral') . '</span>'
            ));

            $markup .= '<nav class="pagination text-center">';
            $markup .= $pagination;
            $markup .= '</nav>';
        endif;
    else :
        $markup = $no_posts;
    endif;

    return $markup;
}
add_shortcode('visceral_query', __NAMESPACE__ . '\\visceral_query_shortcode');

/**
 * Featured Impact Story
 *
 * @return  The markup for a section with a chosen impact story
 */
function featured_post_shortcode($atts)
{
    if (function_exists('get_field') && get_field('featured_post') != '') {
        $featured_post = get_field('featured_post');
        if ('publish' == $featured_post[0]->post_status) {

            $id = $featured_post[0]->ID;
            $post_type = $featured_post[0]->post_type;

            if ($post_type === 'impact_story') {
                $posts_link = '/our-work/impact-stories/';
                $post_link_label = 'Read the Story';
                $posts_label = 'Impact Stories';
                $posts_btn_class = 'btn--blue';
            } elseif ($post_type === 'resource') {
                $posts_link = '/insights-resources/resource-library/';
                $post_link_label = 'View the Resource';
                $posts_label = 'Resource Library';
                $posts_btn_class = 'btn--magenta';
            } elseif ($post_type === 'event') {
                $posts_link = '/events/';
                $post_link_label = 'View the Event';
                $posts_label = 'Events';
                $posts_btn_class = 'btn--purple';
            } else {
                $posts_link = '/insights-resources/insights-perspectives/';
                $post_link_label = 'Read the Insight';
                $posts_label = 'Insights & Perspectives';
                $posts_btn_class = 'btn--orange';
            }

            // full
            $featured_image_bg = get_the_post_thumbnail_url($id, 'medium');

            // 600x400
            $featured_image = get_the_post_thumbnail_url($id, '600x400');

            $markup = '<div class="featured_story">';

            // background
            if ($featured_image_bg) $markup .= '<div class="featured_story__bg" style="background-image: url(' . $featured_image_bg . ');">&nbsp;</div>';

            $markup .= '<div class="container">';
            $markup .= '<div class="row">';

            $markup .= '<div class="column sm-50 reveal">';
            $markup .= '    <p><a href="' . $posts_link . '" class="btn btn--small ' . $posts_btn_class . '">' . $posts_label . '</a></p>';
            $markup .= '    <a href="' . get_permalink($id) . '" class="featured_story__link">';
            $markup .= '    <h3 class="text-white">' . $featured_post[0]->post_title . '</h3>';
            $markup .= '    <p class="">' . $post_link_label . ' <span class="icon icon-chevron-right" aria-hidden="true"></span></p>';
            $markup .= '    </a>';
            $markup .= '</div>'; // .column

            $markup .= '<div class="column sm-50">';
            if ($featured_image) $markup .= '<a href="' . get_permalink($id) . '" class="featured_story__img reveal reveal--left image-zoom"><img src="' . $featured_image . '" class="img-cover" alt="' . $featured_post[0]->post_title . '"/></a>';
            $markup .= '</div>'; // .column

            $markup .= '</div>'; // .row
            $markup .= '</div>'; // .container
            $markup .= '</div>'; // .featured_post
        }
    }

    return $markup;
}
add_shortcode('featured_post', __NAMESPACE__ . '\\featured_post_shortcode');

/**
 * Related Content
 *
 * @return  The markup for a section with a chosen impact story
 */
function related_content_shortcode($atts)
{

    extract(shortcode_atts(array(
        'sector' => '',
        'heading' => 'Learn More'
    ), $atts));

    $related_content = visceral_related_posts(3, array('post', 'event', 'resource', 'impact_story'), $sector, 'related_content');

    $markup = '';

    $markup .= '<section class="related-content">';
    $markup .= '<p class="h6 text-center related-content__heading">' . $heading . '</p>';
    $markup .= '<div class="row">';
    global $post;
    foreach ($related_content as $post) :
        setup_postdata($post);
        $markup .= \App\template('partials.list-item-card');
        wp_reset_postdata();
    endforeach;
    $markup .= '</div>';
    $markup .= '<p class="related-content__all-links text-center">View all: <a href="/insights-resources/resource-library/" class="btn btn--small btn--magenta">Resources</a> <a href="/insights-resources/insights-perspectives/" class="btn btn--small btn--orange">Insights &amp; Perspectives</a> <a href="/our-work/impact-stories/" class="btn btn--small btn--blue">Impact Stories</a> <a href="/events/" class="btn btn--small btn--purple">Events</a></p>';
    $markup .= '</section>';

    return $markup;
}
add_shortcode('related_content', __NAMESPACE__ . '\\related_content_shortcode');

function related_insights_resources_stories_shortcode($atts)
{

    extract(shortcode_atts(array(
        'sector' => '',
        'heading' => 'Learn More'
    ), $atts));

    $related_content = visceral_related_posts(3, array('post', 'event', 'resource', 'impact_story'), $sector, 'related_content');

    $markup = '';

    $markup .= '<section class="related-content">';
    $markup .= '<p class="h6 text-center related-content__heading">' . $heading . '</p>';
    $markup .= '<div class="row">';
    global $post;
    foreach ($related_content as $post) :
        setup_postdata($post);
        $markup .= \App\template('partials.list-item-card');
        wp_reset_postdata();
    endforeach;
    $markup .= '</div>';
    $markup .= '<p class="related-content__all-links text-center">View all: <a href="/insights-resources/resource-library/" class="btn btn--small btn--magenta">Resources</a> <a href="/insights-resources/insights-perspectives/" class="btn btn--small btn--orange">Insights &amp; Perspectives</a> <a href="/our-work/impact-stories/" class="btn btn--small btn--blue">Impact Stories</a></p>';
    $markup .= '</section>';

    return $markup;
}
add_shortcode('related_insights_resources_stories', __NAMESPACE__ . '\\related_insights_resources_stories_shortcode');

function related_events_shortcode($atts)
{

    extract(shortcode_atts(array(
        'sector' => '',
        'heading' => 'Learn More'
    ), $atts));

    $related_content = visceral_related_posts(3, array('event'), $sector, 'related_events');

    $markup = '';

    $markup .= '<section class="related-content">';
    $markup .= '<p class="h6 text-center related-content__heading">' . $heading . '</p>';
    $markup .= '<div class="row">';
    global $post;
    foreach ($related_content as $post) :
        setup_postdata($post);
        $markup .= \App\template('partials.list-item-card');
        wp_reset_postdata();
    endforeach;
    $markup .= '</div>';
    $markup .= '<p class="related-content__all-links text-center">View all: <a href="/events/" class="btn btn--small btn--purple">Events</a></p>';
    $markup .= '</section>';

    return $markup;
}
add_shortcode('related_events', __NAMESPACE__ . '\\related_events_shortcode');



/* END SHORTCODES */

/* GENERAL PURPOSE FUNCTIONS */

/**
 * RELATED POSTS FUNCTION - Creates an array of related post objects
 *
 * @param int   $total_posts                 Total posts to display
 * @param mixed $post_type                   Either a string or array of post types to display
 * @param mixed $taxonomies                  Either a string or array of post taxonomies to display
 * @param string $related_posts_field_name   Name of the manually selected related posts ACF field
 *
 * @return array                             Returns an of related posts
 */
function visceral_related_posts($total_posts = 3, $post_type = 'post', $post_tax = '', $related_posts_field_name = 'related_content')
{
    // Set up the an array to hold them
    $related_posts = array();

    // First let's check ACF. Make sure it's installed and that the field isn't empty
    // Add each post object to our array
    // Subtract each post from total posts
    if (function_exists('get_field') && get_field($related_posts_field_name) != '') {
        foreach (get_field($related_posts_field_name) as $post_object) {
            if ($post_object->post_status === 'publish') {
                array_push($related_posts, $post_object);
                $total_posts--;
            }
        }
    }
    // if we need more posts let's see if there's other content that has it selected.
    if ('experience' == get_post_type() || 'service' == get_post_type()) {
        if (0 < $total_posts) {
            // If related posts were chosen, let's exclude them so we don't add them twice
            foreach ($related_posts as $related_post) {
                $no_include_posts_ids[] = $related_post->ID;
            }

            // This will set the meta key for the content that is related to current service or experience
            $key = ('experience' == get_post_type()) ? 'experiences' : 'services';

            $args = array(
                'post_type'      => $post_type,
                'post_status'    => 'publish',
                'posts_per_page' => $total_posts,
                'post__not_in'   => $no_include_posts_ids,
                'orderby'        => 'post_date',
                'order'          => 'DESC',
                'meta_query'     => array(
                    array(
                        'key'     => $key,
                        'value'   => '"' . get_the_ID() . '"',
                        'compare' => 'LIKE'
                    )
                )
            );

            // Get the posts based on criteria specified above
            $more_posts = get_posts($args);
            foreach ($more_posts as $post_object) {
                array_push($related_posts, $post_object);
                $no_include_posts_ids[] = $post_object->ID;
                $total_posts--;
            }
        }
    }
    // If we still need more posts, let's get some base on most recent
    if (0 < $total_posts) {

        // If related posts were chosen, let's exclude them so we don't add them twice
        foreach ($related_posts as $related_post) {
            $no_include_posts_ids[] = $related_post->ID;
        }
        // This includes the current post we're on
        $no_include_posts_ids[] = get_the_id();
        $args = array(
            'post_type'      => $post_type,
            'post_status'    => 'publish',
            'posts_per_page' => $total_posts,
            'post__not_in'   => $no_include_posts_ids,
            'orderby'        => 'post_date',
            'order'          => 'DESC'
        );

        // Get the posts based on criteria specified above
        $more_posts = get_posts($args);
        foreach ($more_posts as $post_object) {
            array_push($related_posts, $post_object);
            $no_include_posts_ids[] = $post_object->ID;
            $total_posts--;
        }
    }

    return $related_posts;
}
// END RELATED POSTS FUNCTION

/**
 * Takes a string and returns a truncated version. Also strips out shortcodes
 *
 * @param  string  $text   String to truncate
 * @param  integer $length Character count limit for truncation
 * @param  string  $append Appended to the end of the string if character count exceeds limit
 * @return string          Truncated string
 */
function truncate_text($text = '', $length = 150, $append = '...')
{
    $new_text = preg_replace(" ([.*?])", '', $text);
    $new_text = strip_shortcodes($new_text);
    $new_text = strip_tags($new_text);
    $new_text = substr($new_text, 0, $length);
    if (strlen($new_text) == $length) {
        $new_text = substr($new_text, 0, strripos($new_text, " "));
        $new_text = $new_text . $append;
    }

    return $new_text;
}

/**
 * Returns an image with a class for fitting to a certain aspect ratio
 *
 * @param  integer $aspect_ratio_width   Aspect Ratio Width
 * @param  integer $aspect_ratio_height   Aspect Ratio Height
 * @param  string  $image_size   Size of featured image to return
 * @return string  markup of image
 */
function get_aspect_ratio_image($aspect_ratio_width = 1, $aspect_ratio_height = 1, $image_size = 'medium')
{
    $image = wp_get_attachment_image_src(get_post_thumbnail_id(), $image_size);
    $w     = $image[1];
    $h     = $image[2];
    $class = ((isset($h) && isset($w)) && ($h / $w) > ($aspect_ratio_height / $aspect_ratio_width)) ? 'portrait' : '';
    return get_the_post_thumbnail(get_the_id(), $image_size, array('class' => $class));
}

/**
 * Checks the current page and returns true or false if it should have a masthead or not.
 *
 * @return boolean	Whether the current page should have a masthead or not.
 */
function visceral_has_masthead()
{

    if (is_singular(array('post', 'person', 'service', 'experience', 'resource', 'event', 'product')) || is_search()) {
        return false;
    } elseif (basename(get_page_template()) == "template-no-masthead.blade.php") {
        return false;
    } elseif (is_front_page() || is_page('events') || is_page('resource-library') || is_page('insights-perspectives') || is_404()) {
        return false;
    } else {
        return true;
    }
}

/**
 * LIMIT EXCERPT LENGTH
 */

function visceral_get_the_excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`', '', $excerpt);
    return $excerpt;
}

// END LIMIT EXCERPT LENGTH

// Obfuscate Email Addresses
function obfuscateEmail($email) {
    $obfuscated = '';
    for ($i = 0; $i < strlen($email); $i++) {
        $obfuscated .= '\x' . dechex(ord($email[$i]));
    }
    return $obfuscated;
}


/**
 * Menu ACF - Filters menu items HTML with an optional
 * image and caption (via ACF). These are only meant to be displayed
 * on the "mega nav" dropdown on desktop.
 *
 * @return  The filtered output of a navigation menu's items
 * 
 */
function acf_nav_menu($sorted_menu_items, $args)
{

    if (!empty($sorted_menu_items)) {

        foreach ($sorted_menu_items as $item) {
            if (function_exists('get_field')) {
                $caption = get_field('menu_item_text', $item);
                $page    = $item->title;

                $item->title = '';

                if ($caption) {
                    $item->title .= '<span class="menu-item--title">' . $page . '</span><span class="menu-item--caption">' . $caption . '</span>';
                    //   $item->title .= '<span class="menu__title menu__title--page">' . $page . '</span>';
                } else {
                    $item->title .= '<span class="menu-item--title">' . $page . '</span>';
                }
            }
        }
    }

    return $sorted_menu_items;
}
add_filter('wp_nav_menu_objects',  __NAMESPACE__ . '\\acf_nav_menu', 10, 2);


/**
 * Extend default Walker_Nav_Menu
 * to adjust HTML for main navigation sub-menus
 *
 * @return wp_nav_menu
 * 
 */
class visc_walker extends \Walker_Nav_Menu
{
    // add classes to ul sub-menus
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        // depth dependent classes
        $indent        = ($depth > 0  ? str_repeat("\t", $depth) : ''); // code indent
        $display_depth = ($depth + 1); // because it counts the first submenu as 0
        $classes       = array(
            'header__sub-menu',
            'menu-depth-' . $display_depth
        );
        $class_names = implode(' ', $classes);

        // build html
        if ($display_depth == 1) {
            $output .= "\n" . $indent . '<ul class="' . $class_names . '"><div class="container"><div class="row">' . "\n";
        } else {
            $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
        }
    }
}

class visc_walker_mobile extends \Walker_Nav_Menu
{
    // add classes to ul sub-menus
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        // depth dependent classes
        $indent        = ($depth > 0  ? str_repeat("\t", $depth) : ''); // code indent
        $display_depth = ($depth + 1); // because it counts the first submenu as 0
        $classes       = array(
            'mobile-nav__sub-menu',
            'menu-depth-' . $display_depth
        );
        $class_names = implode(' ', $classes);

        // build html
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }
}

/**
 * Updates a meta field with author
 */
function visc_create_author_meta($post_id)
{
    $post_type = get_post_type($post_id);
    if (in_array($post_type, array('post', 'resource', 'event'))) {
        if ($post_type === 'post') {
            $meta_field = 'authors';
        } elseif ($post_type === 'resource') {
            $meta_field = 'people';
        } elseif ($post_type === 'event') {
            $meta_field = 'related_people';
        }
        if (metadata_exists('post', $post_id, 'people_names_dynamic')) {
            $people_names = '';
            if (function_exists('get_field') && !empty(get_field($meta_field))) {
                $people = get_field($meta_field);
                foreach ($people as $person) {
                    $ID = $person->ID;
                    $people_names .=  $person->post_title . ' ';
                }
            }

            if ($post_type === 'post') {
                if (function_exists('get_field') && !empty(get_field('custom_authors'))) {
                    $custom_authors = get_field('custom_authors');
                    foreach ($custom_authors as $custom_author) {
                        // Check if name provided (don't show image if no name)
                        if ($custom_author['author_name'] !== "") {
                            $people_names .= $custom_author['author_name'] . ' ';
                        }
                    }
                }
            }
            update_post_meta($post_id, 'people_names_dynamic', $people_names);
        }
    }
}

add_action('save_post', __NAMESPACE__ . '\\visc_create_author_meta');

// WooCommerce Overrides

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

function add_shop_cart_link()
{
    echo '<a href="/cart" class="visc-wc-cart-link"><i class="icon icon-cart"></i>' . __('View Cart', 'visceral') . '</a>';
}

add_action('woocommerce_before_shop_loop', __NAMESPACE__ . '\\add_shop_cart_link', 30);

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

add_action('woocommerce_after_shop_loop_item_title', function () {
    woocommerce_template_single_excerpt();
});

function visc_remove_woocommerce_loop_button()
{
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
}
add_action('init', __NAMESPACE__ . '\\visc_remove_woocommerce_loop_button');


function visc_replace_add_to_cart()
{
    global $product;
    $link = $product->get_permalink();
    echo do_shortcode('<a href="' . esc_attr($link) . '" class="button">View</a>');
}

add_action('woocommerce_after_shop_loop_item', __NAMESPACE__ . '\\visc_replace_add_to_cart');

function remove_image_zoom_support()
{
    remove_theme_support('wc-product-gallery-zoom');
    remove_theme_support('wc-product-gallery-lightbox');
    remove_theme_support('wc-product-gallery-slider');
}
add_action('template_redirect', __NAMESPACE__ . '\\remove_image_zoom_support', 100);


// Custom Order Confirmation Page Title
function woo_title_order_received($title, $id)
{
    if (
        function_exists('is_order_received_page') &&
        is_order_received_page() && get_the_ID() === $id
    ) {
        $custom_thank_you_page = get_page_by_path('products/custom-thank-you');

        $title = $custom_thank_you_page->post_title;
    }
    return $title;
}

add_filter('the_title', __NAMESPACE__ . '\\woo_title_order_received', 10, 2);


// Custom Order Confirmation Page content
function visc_custom_order_thankyou($thankyoutext, $order)
{
    $custom_thank_you_page = get_page_by_path('products/custom-thank-you');
    $custom_thank_you_page_content = do_shortcode($custom_thank_you_page->post_content);
    $output = $custom_thank_you_page_content;
    return $output;
}

add_filter('woocommerce_thankyou_order_received_text_after', __NAMESPACE__ . '\\visc_custom_order_thankyou', 10, 2);

function visc_custom_order_thankyou_before($thankyoutext, $order)
{
    return '';
}

add_filter('woocommerce_thankyou_order_received_text', __NAMESPACE__ . '\\visc_custom_order_thankyou_before', 10, 2);
